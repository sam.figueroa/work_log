# 2025-03-05
- process todos
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/183484
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/382338#note_2380305664
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/521321
- update: https://gitlab.com/gitlab-org/software-supply-chain-security/compliance/general/-/issues/354#note_2380992637
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174039

# 2025-03-04
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/182877
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/182870
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174039
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/182712

# 2025-02-28
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/28973#note_2372232371
- meeting: coffee chat w/ Lohit Peesapati
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/182870
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/182877

# 2025-02-27
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/182538#note_2370089779
- update: https://gitlab.com/gitlab-org/quality/engineering-productivity/approved-mr-pipeline-incidents/-/issues/1565#note_2370131510
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/182870
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/182877

# 2025-02-26
- process emails
- investigate: https://new-sentry.gitlab.net/organizations/gitlab/issues/1227284/events/14a8bfb556434dcb9ae5f846e85b71fa/
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/182712
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/512381
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/182719
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/182751
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174039

# 2025-02-25
- meeting: 1:1 w/ Nate
- meeting: coffee chat w/ Inma Checa
- meeting: Compliance Weekly
- meeting: coffee chat w/ Jörn
- investigate: https://gitlab.com/gitlab-org/gitlab/-/issues/503968#note_2364450118

# 2025-02-24
- review: https://gitlab.com/gitlab-org/software-supply-chain-security/compliance/engineering/snowflake-connector/-/merge_requests/15
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174039

# 2025-02-21
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/507239
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/177748
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174039

# 2025-02-20
- process emails
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/181991
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/431346#note_2358003721
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/179922#note_2357860100
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/179276
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174039

# 2024-12-19
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/173142
- update: https://gitlab.com/gitlab-com/gl-security/corp/issue-tracker/-/issues/1067#note_2267694544

# 2024-12-18
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174996

# 2024-12-17
- prepare 1:1 notes
- meeting: 1:1 w/ Nate
- meeting: 1:1 w/ Ian
- prepare to host bi-weekly
- meeting: Compliance Bi-Weekly
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174996

# 2024-12-16
- update MacOS
- update: https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/10004
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/165212
- process slack
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/470695#note_2247726052
- update: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/137438

# 2024-12-11
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/175107

# 2024-12-10
- meeting: 1:1 w/ Nate
- meeting: Compliance Weekly
- meeting: Works Council GitLab GmbH

# 2024-12-09
- finish: https://gitlab.com/gitlab-org/gitlab/-/issues/499047
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174484

# 2024-12-05
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/480172#note_2243719002
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174857

# 2024-12-04
- meeting: 1:1 w/ Nate
- meeting: Compliance Weekly
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/464122
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/172491#note_2239246952
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/173885

# 2024-12-02
- update: https://gitlab.com/gitlab-org/software-supply-chain-security/compliance/general/-/issues/335
- investigate sentry errors
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174437
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/478560#note_2236830328
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/506655

# 2024-11-26
- update: https://gitlab.com/gitlab-org/software-supply-chain-security/compliance/general/-/issues/327#note_2227940903
- meeting: 1:1 w/ Nate
- meeting: Compliance Weekly
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/480117
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/169338

# 2024-11-25
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/169338
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/480117
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/173747
- planning: https://gitlab.com/gitlab-org/gitlab/-/issues/464122

# 2024-11-18
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/173010
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/172757#note_2216442265
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/173019

# 2024-11-15
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/172850
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/433329#note_2214184296
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/374110#note_2214163749
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/172757

# 2024-11-14
- handle: update Workday profile request
- catch up on todos
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/171745
- fix GDK
- update: https://gitlab.com/gitlab-data/analytics/-/issues/22111#note_2210394395

# 2024-11-13
- out sick

# 2024-11-12
- out sick

# 2024-11-11
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/480460

# 2024-11-08
- process email
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/171720

# 2024-11-07
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/170380
- slack discussions & research

# 2024-11-06
- meeting: pair programming w/ Smriti
- upadate: https://gitlab.com/groups/gitlab-org/-/epics/7917
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/481325
- start: https://gitlab.com/gitlab-org/gitlab/-/issues/440922
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/171851

# 2024-11-05
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/171345
- meeting: 1:1 w/ Nate
- meeting: coffee chat w/ Yasha Rise
- meeting: Compliance Weekly
- update: https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/8670
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/171181
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/171675
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/165160

# 2024-11-04
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/170200
- fix GDK
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/171030
- read: Engineering Week in Review
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/170971

# 2024-10-31
- process feedback: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168865

# 2024-10-30
- process feedback: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168865
- meeting: Sec coffee chat

# 2024-10-28
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/143914

# 2024-10-29
- meeting: 1:1 w/ Nate
- finish cleanup: ff_compliance_audit_mr_merge
- meeting: Compliance Weekly
- process feedback and fix CI: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168865
- meeting: coffee chat w/ Andrew Jung

# 2024-10-24
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/169485
- fix CI for: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168865
- complete: data privacy course
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/170090
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/170450

# 2024-10-23
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/169892
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/472145
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/169165
- request review on: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168865

# 2024-10-22
- record: Compliance demo
- edit: Compliance demo
- upload: https://www.youtube.com/watch?v=GrxitnQbkq4
- meeting: 1:1 w/ Nate
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/472145
- meeting: Compliance Weekly
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/169165

# 2024-10-21
- enable ff_compliance_audit_mr_merge globally on gprd
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/472145
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/169892

# 2024-10-18
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/169287
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/169452
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/472145
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/482720#note_2165891117
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/442279#note_2165931196

# 2024-10-17
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/472145

# 2024-10-16
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/169330
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/169352
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168778
- deploy: feature flag for https://gitlab.com/gitlab-org/gitlab/-/issues/442279
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/472145

# 2024-10-15
- setup local k8s
- review: https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/4535
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168626
- prepare 1:1 agenda
- meeting: 1:1 w/ Nate
- meeting: Timo Furrer
- prepare to host Compliance Weekly
- meeting: Compliance Weekly
- review: https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/4535
- attempted to setup k8s on another copy of GDK that already has HTTPS, failed
- setup HTTPS on main GDK
- meeting: Timo Furrer

# 2024-10-14
- finish: https://gitlab.com/gitlab-org/gitlab/-/issues/442279
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168799
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168626
- review: https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/4535
- setup local k8s

# 2024-10-11
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/472145
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/442279

# 2024-10-10
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/442279
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/472145

# 2024-10-09
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168508
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/442279

# 2024-10-08
- prepare 1:1 agenda
- meeting: 1:1 w/ Nate
- meeting: Compliance Weekly
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/442279

# 2024-10-07
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/167933
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/167021
- proccess todos
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/492317#note_2145012358
- watch: https://www.youtube.com/watch?v=gjXMdP7YHDU
- update OS
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/442279

# 2024-10-04
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/167510
- review: https://gitlab.com/gitlab-org/govern/compliance/engineering/snowflake-connector/-/merge_requests/12

# 2024-10-02
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/480694

# 2024-10-01
- meeting: 1:1 w/ Nate
- meeting: Compliance Weekly
- record: Compliance Demo
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/492317#note_2138733092
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/488894#note_2138738088
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/167510
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/442279

# 2024-09-03
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/163732
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/163854
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/442279
- create: https://gitlab.com/gitlab-org/gitlab/-/issues/482720

# 2024-09-02
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/442279

# 2024-07-19
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/424387N
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/155754
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/468262
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/472405

# 2024-07-18
- provide context for: https://gitlab.zendesk.com/agent/tickets/550180
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/155754

# 2024-07-17
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/155754
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159472
- update: Value based tracker doc
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/467931#note_2003417870
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/159244

# 2024-06-10
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/155791
- process email
- process slack
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/424387

# 2024-06-03
- review: https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/4017
- update: https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/6126

# 2024-05-29
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/150710
- update: https://gitlab.com/groups/gitlab-org/-/epics/12035#note_1926690057
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/153923
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/149449#note_1926760349
- meeting: Kaffeekraenzchen

# 2024-05-28
- meeting: coffee chat w/ James
- meeting: 1:1 w/ Nate
- meeting: Compliance Weekly
- meeting: coffee chat w/ Marion Daire
- meeting: coffee caht w/ Alexander Turniske, no show
- process emails

# 2024-05-27
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/154255
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/149449
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/153923
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/153886
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/154158
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/150710

# 2024-05-21
- meeting: 1:1 w/ Nate
- meeting: Compliance Weekly
- fix GDK
- review: https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/4067
- re-review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/153300

# 2024-05-17
- update: DE works council election slides
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/153300
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/153265
- open: https://gitlab.com/gitlab-com/content-sites/handbook/-/merge_requests/5904

# 2024-05-16
- process slack
- process email
- complete CoC & Ethics training
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/150710

# 2024-05-15
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/150710
  - build is still red
  - process MR feedback from technical writing counterpart
- read: https://gitlab.com/gitlab-org/govern/compliance/engineering/snowflake-connector
- meeting: Snowflake connector walkthrough
- watch: https://www.youtube.com/watch?v=V1hUlFOBbYY
- update GDK
- read: https://gitlab.com/groups/gitlab-org/-/epics/12465
- read: https://gitlab.com/groups/gitlab-org/-/epics/12469
- read: https://docs.gitlab.com/ee/development/internal_analytics/internal_event_instrumentation/quick_start.html#defining-event-and-metrics

# 2024-04-30
- update: https://gitlab.com/gitlab-com/content-sites/handbook/-/merge_requests/5120
- meeting: 1:1 w/ Nate
- meeting: Compliance Weekly
- meeting: coffee chat w/ Joern Schneeweisz
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/149449
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/431346

# 2024-04-25
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/431346
- start: https://gitlab.com/gitlab-org/gitlab/-/issues/458087
- update: https://gitlab.com/gl-retrospectives/govern/compliance/-/issues/47
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/150406
- create: backport MRs

# 2024-04-24
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/431346
- start: https://gitlab.com/gitlab-org/gitlab/-/issues/457112
- meeting: coffee chat w/ Nataliia Radina
- meeting: Kaffeekraenzchen
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/150699

# 2024-04-23
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/431346
- meeting: coffee chat w/ Ian
- meeting: 1:1 w/ Nate
- meeting: Compliance weekly

# 2024-04-12
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/147855
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/149392
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/148080#note_1859453405
- push sec fix review

# 2024-04-11
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/140484
- push sec fix review

# 2024-04-10
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/148388
- push sec fix review

# 2024-04-09
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/148947
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/147904
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/146917
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/148608
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/149001
- push sec fix review
- investigate lefthook uninstall on security harness
- process slack

# 2024-04-08
- push sec fix review
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/148764
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/148648
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/148383

# 2024-03-26
- meeting: 1:1 w/ Nate
- prepare for hosting Compliance weekly meeting
- meeting: Compliance weekly
- meeting: coffee chat w/ Simon Knox
- meeting: GitLab GmbH - company assembly - work council
- meeting: coffee chat w/ Doug
- meeting: coffee chat w/ Kelley Stowe/Gerret

# 2024-03-25
- meeting: coffee chat w/ Ian Khor

# 2024-03-22
- process emails
- complete: https://university.gitlab.com/courses/the-customer-ready-shadow-program-ent-100-test

# 2024-03-05
- meeting: 1:1 w/ Nate
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/441197
  - test group level audit event streaming for Cells POC
  - test SAML SSO MR approval for Cells POC

# 2024-02-26
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/424387
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/145509
- process emails
- review: https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/3899


# 2024-02-23
- continue: NN-zero to hero course

# 2024-02-22
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/145302

# 2024-02-21
- meeting: backend pairing
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/424387
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/145302
- meeting: Sec Section Coffee Chat

# 2024-02-20
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/424387

# 2024-02-19
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/145112
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/144915
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/424387
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/145169

# 2024-02-15
- update GDK
- process emails
- start: https://gitlab.com/gitlab-org/gitlab/-/issues/421636
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/144862
- start: https://gitlab.com/gitlab-org/gitlab/-/issues/424387

# 2024-02-14
- out sick

# 2024-02-13
- fill out: growth plan
- prepare: 1:1 agenda
- meeting: 1:1 w/ Nate
- meeting: coffee chat w/ Dominique Top
- meeting: coffee chat w/ Sarina Kraft
- meeting: coffee chat w/ Olivier Gonzales
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/144580
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/142406

# 2024-02-12
- process emails
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/187
- update: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/132925
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/431714
- complete: SAFE training

# 2024-02-09
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/426660#note_1765842366
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/441092#note_1765806928
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/440923
- start clean up: https://gitlab.com/gitlab-org/gitlab/-/issues/431714

# 2024-02-08
- meeting: 1:1 w/ Nate
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/435713
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/144043

# 2024-02-07
- meeting: backend pairing
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/112097
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/435713

# 2024-02-06
- close: https://gitlab.com/gitlab-org/gitlab/-/issues/424447
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/143247
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/435713

# 2024-02-02
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/426660#note_1755221932
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/143530
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/143577
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/143397
- meeting: compliance pairing

# 2024-02-01
- create feature flag issues for compliance pipeline maintenance & deprecation

# 2024-01-31
- meeting: coffee chat w/ Hitesh
- meeting: Kaffeekraenzchen
- finish: compliance pipeline maintenance & deprecation plan
- process emails

# 2024-01-29
- continue: plan compliance pipeline maintenance & deprecation
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/141739

# 2024-01-26
- meeting: code review weekly
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/142832

# 2024-01-22
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/142100
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/142355
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/142389

# 2024-01-19
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/142039

# 2024-01-18
- meeting: 1:1 w/ Nate
- meeting: Compliance Bi-Weekly

# 2024-01-17
- meeting: backend pairing
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/141963
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/141964

# 2024-01-16
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/436690#note_1713663844
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/435404#note_1727872073
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/141532

# 2024-01-15
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/139912
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/141745
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/140979
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/141710
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/435404#note_1727872073

# 2024-01-10
- meeting: backend pairing
- create: https://gitlab.com/rverschoor/punk/-/merge_requests/16
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/141347
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/435404
  - investigate: ForceAuthn

# 2024-01-09
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/435404

# 2024-01-08
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129608

# 2024-01-05
- meeting: code review workshop
- coffee chat: Artur Fedorov
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/437282

# 2024-01-04
- file expenses
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/435519#note_1713104351
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/136989#note_1713535475
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/436690#note_1713663844

# 2024-01-03
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/431346#note_1712773648
- update: https://gitlab.com/gl-retrospectives/govern/compliance/-/issues/41#note_1712761459
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/436690#note_1712751222
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/136989
- investigate 500 timeout error on audit event dashboard

# 2024-01-02
- process emails
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/427187
- update: https://gitlab.com/gl-retrospectives/govern/compliance/-/issues/42
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/435999
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/426660
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/180
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/428053#note_1675099888
- read: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/138489
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/418197
- process slack
- coffee chat: Caroline Hämming

# 2023-12-18
- investigate: https://gitlab.com/gitlab-org/gitlab/-/issues/8070#note_1685064767
- created: https://gitlab.com/gitlab-org/gitlab/-/issues/435713

# 2023-12-11
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/431415
- meeting: pairing on Instance SAML approval

# 2023-12-10
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/431415

# 2023-12-07
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/431415

# 2023-12-06
- meeting: backend pairing
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/431415

# 2023-12-01
- meeting: compliance pairing
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/431415
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/137067

# 2023-11-30
- write implementation plan for instance SAML MR approval
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/137067

# 2023-11-29
- meeting: backend pairing
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/137893
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/137067
- write implementation plan for instance SAML MR approval
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/165
- update: https://gitlab.com/gl-retrospectives/govern/compliance/-/issues/41
- update: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/167

# 2023-11-28
- meeting: coffee chat w/ James Hebbden
- meeting: 1:1 w/ Nate
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/136989#note_1661072552
- write implementation plan for instance SAML MR approval
- meeting: coffee chat w/ Alex Hanselka

# 2023-11-20
- update: https://gitlab.com/gl-retrospectives/govern/compliance/-/issues/41
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/137067

# 2023-11-10
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959

# 2023-11-09
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959

# 2023-11-08
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/136310

# 2023-11-07
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959

# 2023-11-06
- process toodos
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/135672
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959

# 2023-11-03
- meeting: 1:1 w/ Nate
- meeting: compliance pairing
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959

# 2023-11-02
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/135830
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- update: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/158#note_1631191427

# 2023-10-31
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959

# 2023-10-30
- open: https://gitlab.com/gitlab-org/gitlab/-/issues/429807
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/411610
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/156#note_1625424282

# 2023-10-27
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/135080
- meeting: compliance pairing
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/135390
- start work for compare-shortcut

# 2023-10-26
- process feedback: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129608

# 2023-10-25
- meeting: backend pairing
- meeting: Compliance Bi-Weekly
- process feedback: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129608

# 2023-10-24
- meeting: 1:1 w/ Nate
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- participate in: GitLab Duo Chat Bash

# 2023-10-23
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- create: Compliance violation export demo https://www.youtube.com/watch?v=qzAAxPBAJAA
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/422452
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/130911

# 2023-10-20
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- meeting: compliance pairing

# 2023-10-19
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/133648
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/423263#note_1611079849
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/423263#note_1611079849
- https://gitlab.com/gitlab-org/gitlab/-/issues/428340

# 2023-10-18
- coffee chat: James Hebden
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- meeting: backend pairing
- meeting: Sec seciton social call

# 2023-10-17
- coffee chat: w/ Phil
- meeting: 1:1 w/ Nate
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- meeting: Book Club: Impact Players
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/134047

# 2023-10-16
- complete: training for "The AI-powered DevSecOps platform" in LevelUp
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/134239
- fill: talent assesment doc
- process emails
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959

# 2023-10-13
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- roll out FF for violation export to 90%

# 2023-10-12
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- roll out FF for violation export to 75%

# 2023-10-11
- meeting: backend pairing

# 2023-10-10
- prepare: Compliance Sync agenda
- meeting: 1:1 w/ Nate
- meeting: Compliance Bi-Weekly
- process notes from 1:1
- roll out FF for violation export to 25%
- review: !129702
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- meeting: Book Club: Impact Players

# 2023-10-09
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/377762
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/423263

# 2023-10-05
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959

# 2023-10-04
- meeting: backend pairing
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959

# 2023-09-28
- update: https://gitlab.com/gl-retrospectives/govern/compliance/-/issues/39

# 2023-09-27
- read: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/132208
- meeting: backend pairing
- create: https://gitlab.com/gitlab-org/gitlab/-/issues/426413
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/259159

# 2023-09-26
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129608
- meeting: 1:1 w/ Nate
- read: Engineering All Hands notes
- activate: compliance_violation_csv_export FF for test group
- meeting: book club Impact Players

# 2023-09-25
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/132549
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129608

# 2023-09-21
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/132273
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/131882

# 2023-09-20
- coffee chat: James Hebbden
- meeting: backend pairing
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/132273
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/132157
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/356791#note_1567937773

# 2023-09-19
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/141
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/131088
- update: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/128184#note_1564417445
- meeting: book club Impact Players
- meeting: 1:1 w/ Nate
- coffee chat: Kamil
- hack: compliance hackathon

# 2023-09-18
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/131088
- hack: compliance hackathon
  - https://gitlab.com/gitlab-org/gitlab/-/merge_requests/131882

# 2023-09-15
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/131882

# 2023-09-14
- investigate broken master
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/124811
- coffee chat: Hitesh

# 2023-09-13
- meeting: backend pairing
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/120636
- update: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/129331
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/424968#note_1556725848
- coffee chat: Fabiene Catteau

# 2023-09-12
- prepare: Compliance Sync agenda
- meeting: 1:1 w/ Nate
- meeting: Compliance Bi-Weekly
- meeting: compliance pairing
- add Compliance Center metrics to SiSense
- update: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/129331

# 2023-09-11
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/423263#note_1552709447
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/377762#note_1552755784
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/377769#note_1552757611
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/422292
- post status updates
- process DB exceptions in slack

# 2023-09-08
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/130726
- update: MacOS
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/131279

# 2023-09-07
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/131084
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/120636
- create: https://gitlab.com/gitlab-org/gitlab/-/issues/424447
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/131206
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/130726

# 2023-09-06
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/120636
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129532
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/125512
- test SiSense access toSnowplow data & Compliance Dashboards
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129699
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/131084
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/131088

# 2023-09-05
- process emails
- process slack
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/120636
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/423263#note_1544134017
- coffee chat: w/ Sean

# 2023-09-01
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/130599
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/128148

# 2023-08-31
- process emails
- process slack
- update: https://gitlab.com/gitlab-com/sec-sub-department/section-sec-request-for-help/-/issues/85#note_1536401946
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/128148
- install new GDK and test Vite
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/423851#note_1537406489

# 2023-08-30
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/370169#note_1534693904
- meeting: backend pairing

# 2023-08-29
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/141#note_1531457373
- fix: https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/3523
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/130079

# 2023-08-28
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/130079
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- process feedback on open MRs
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/370169

# 2023-08-24
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/370169
- reveiw: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129899
- start: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/415403#note_1524363582
- upate: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129699

# 2023-08-23
- meeting: backend pairing
- continue: setup: local SAML IdP
- create: https://gitlab.com/gitlab-org/gitlab/-/issues/370169
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129780
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/408151

# 2023-08-22
- update: https://gitlab.com/gl-retrospectives/govern/compliance/-/issues/38
- update: https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/3873
- prepare 1:1 agenda
- meeting: 1:1 w/ Nate
- check progress on open MRs
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/408151
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/128313
- process emails
- update macOS

# 2023-08-21
- process emails
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/141#note_1520257178
- start: https://gitlab.com/gitlab-org/gitlab/-/issues/421944
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/128148
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129699
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129261

# 2023-08-18
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129595
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129532
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/123516

# 2023-08-17
- update: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/136#note_1516494958
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/408153
- watch: FY24-Q3 Quarterly Kickoff AMA
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129261
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/250663

# 2023-08-16
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/250663#note_1514887991
- plan: https://gitlab.com/gitlab-org/gitlab/-/issues/250663
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129261
- meeting: compliance pairing

# 2023-08-15
- setup: local SAML IdP
- meeting: 1:1 w/ Nate
- meeting: Compliance Bi-Weekly
- coffee chat: Cassiana
- create: https://gitlab.com/gitlab-org/gitlab-development-kit/-/merge_requests/3319

# 2023-08-14
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/128656
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129261+s
- update: https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/3873
- read: https://gitlab.com/gitlab-com/enablement-sub-department/section-enable-request-for-help/-/issues/34
- read: https://gitlab.com/gitlab-org/govern/compliance/compliance-management/features-priorities-automation
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/419733
- create: https://gitlab.com/gitlab-org/gitlab/-/issues/421961
- create: https://gitlab.com/gitlab-org/gitlab/-/issues/421960
- create: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- create: https://gitlab.com/gitlab-org/gitlab/-/issues/421944
- prepare 1:1 agenda

# 2023-08-11
- edit: https://youtu.be/xeuSp8in8us
  Enhanced audio for clarity
- https://gitlab.com/gitlab-org/gitlab/-/issues/377625

# 2023-08-10
- hack: compliance hackathon
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/128978

# 2023-08-09
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/408153
- meeting: backend pairing
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/370169

# 2023-08-08
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/126#note_1502229199
- read: CTO Office Hours notes for 2023-08-02
- prepare 1:1 agenda
- meeting: 1:1 w/ Nate
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/409465#note_1504544636
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/128134
- read: https://clickhouse.com/docs/en/cloud/reference/architecture
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/123#note_1501267278
- coffee chat: Cassiana, no show
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/370169

# 2023-08-07
- continue verify: https://gitlab.com/gitlab-org/gitlab/-/issues/417433
- create: https://gitlab.com/gitlab-org/gitlab/-/issues/421170
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/419733

# 2023-08-04
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/417433#note_1498889277
- continue: NN-zero to hero course

# 2023-08-03
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/98438
- meeting: compliance paring
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/128147

# 2023-08-02
- meeting: backend pairing EMEA/APAC
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/411557
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/98438

# 2023-08-01
- prepare 1:1 agenda
- meeting: 1:1 w/ Nate
- meeting: Compliance Bi-Weekly
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/127928
- setup snowplow
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/128148

# 2023-07-31
- read: https://docs.gitlab.com/ee/development/secure_coding_guidelines.html#logging
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/123423
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/127928
- update: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/123#note_1493339886
- process emails
- process slack
- read: https://gitlab.com/gitlab-com/books-clubs/combined-book-clubs/-/issues/55
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/128
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/126#note_1493146211

# 2023-07-26
- meeting: backend pairing
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/419733

# 2023-07-25
- prepare 1:1 agenda
- meeting: 1:1 w/ Nate
- watch: https://youtu.be/PRSPQvbFquU
- meeting: coffee chat w/ Chinmay Mehrotra

# 2023-07-24
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/126
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/126930
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/98438
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/377725

# 2023-07-21
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/370169
- watch: https://learn.gitlab.com/ai-devsecops-gcp/ai-fireside-chat-gcp
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/259159
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/98438

# 2023-07-20
- watch: https://www.youtube.com/watch?v=CW0SujsABrs
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/126930
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/125870
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/259159
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/370169

# 2023-07-19
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/377725
- read: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/79876
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/367539
- read: https://www.ecfr.gov/current/title-21/chapter-I/subchapter-A/part-11/subpart-C
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/125870
- read: https://gitlab.com/gitlab-org/editor-extensions/gitlab.vim/-/blob/main/README.md

# 2023-07-18
- process emails
- prepare: Compliance Sync agenda
- meeting: 1:1 w/ Nate
- meeting: Compliance Bi-Weekly
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-07-17
- create: https://gitlab.com/gitlab-org/gitlab/-/issues/418762
- process emails
- process slack
- update GDK
- prepare 1:1 agenda
- read: https://gitlab.com/gl-retrospectives/govern/compliance/-/issues/36#note_1450358030
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-06-29
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791
- create: https://gitlab.com/gitlab-com/anti-abuse-govern-growth-sub-dept/-/issues/102

# 2023-06-28
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/415194
- fill out: 16.1 retrospective
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-06-27
- process emails
- fill out: mid-year check-in
- prepare: 1:1
- meeting: 1:1 w/ Nate
- update: MacOS, Zoom, GDK, Chrome
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/415194
  - revisit: not on gprd yet
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-06-26
- read: https://medium.com/alan/ctes-optimization-8c082efc47ef
- read: https://docs.gitlab.com/ee/development/database/efficient_in_operator_queries.html
- investigate: triage-ops assignment prefrence to Jay and me for verification
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/124567
- investigate: https://gitlab.com/gitlab-org/gitlab/-/issues/393960#note_1437362050
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-06-22
- read: https://github.com/exAspArk/batch-loader/blob/master/README.md
- read: https://docs.gitlab.com/ee/development/database/pagination_guidelines.html#keyset-pagination
- read: https://docs.gitlab.com/ee/development/graphql_guide/batchloader.html
- update: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/113#note_1440988143
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/124268
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/124177
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/122931

# 2023-06-21
- process emails
- process slack
- review blog post: https://docs.google.com/document/d/1lE-hVopuyY_gd6bIXoHBXmfeqQHnJvfry3yz167t7yc
- read: https://docs.gitlab.com/ee/development/testing_guide/best_practices.html
- read: https://github.com/flyerhzm/bullet/blob/main/README.md
- read: https://docs.gitlab.com/ee/development/database/query_recorder.html
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/122931

# 2023-06-20
- process email
- file expenses
- prepare 1:1 agenda
- meeting: 1:1 w/ Nate
- meeting: 1:1 w/ Camelia
- meeting: Compliance Bi-Weekly
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/123683

# 2023-06-19
- process email
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/117
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/411591
- update: https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/2593
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/123683
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/120636
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/124084

# 2023-06-16
- process review feedback
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/374110
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/123683
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/120636
- update: https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/2593

# 2023-06-15
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/413547#note_1432172503
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/123696#note_1432468464
- meeting: compliance pairing
- coffee chat: Doug
- process todos
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/408156#note_1433082485

# 2023-06-14
- coffee chat: Charlie
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/122521
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/123683

# 2023-06-13
- prepare 1:1 agenda
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/374110
- meeting: 1:1 w/ Nate
- read: essentialism for 15 min
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-06-12
- update: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/113
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/122255
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/122746
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/374110
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/109#note_1426412594
- complete: self-managed stops refresher https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/3163

# 2023-06-07
- process slack
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/113
- update: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/113#note_1422001468
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-06-06
- plan 1:1 agenda
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/404730#note_1419393058
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/109#note_1418976073
- meeting: Compliance Bi-Weekly
- process emails
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/410311
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/122374
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-06-05
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791
- evaluate: https://gitlab.com/gitlab-com/gitlab-team-member-socials/-/issues/149
- verified: https://gitlab.com/gitlab-org/gitlab/-/issues/408495
- complete: annual security training

# 2023-06-02
- complete: Neurodiversity in the Workplace training
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-06-01
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/120997#note_1413514378
- complete: AI Vision & Strategy Training
- update: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/104#note_1413888362
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/404371
- complete: secure code training
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/410417#note_1414074963
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/356791#note_1414035648
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-05-31
- setup: okta https://gitlab.com/gitlab-com/it/end-user-services/issues/it-help-issue-tracker/-/issues/317
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/121739
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/111
- update: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/111#note_1412214312
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/121602
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/413547#note_1412348953
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791
- meeting: Kaffeekraenzchen
- coffee chat: Derek Ferguson
- meeting: AI Standup

# 2023-05-30
- meeting: 1:1 w/ Nate
- coffee chat: Hoossam Hamdy
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/121739
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-05-25
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/121739
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/356791
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/119891
- pairing: w/ Hitesh

# 2023-05-24
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791
- investigate UI bug
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/85499#note_1403718459
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/121067#note_1403851122
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/121739

# 2023-05-23
- watch: FY24-Q2 Quarterly Kickoff
- watch: GitLab - AI Fireside Chat https://www.youtube.com/watch?v=ejWeMdVz8Nk
- read: essentialism for 15 min
- read: Engineering Week in Review
- meeting: Compliance Bi-Weekly
- meeting: 1:1 w/ Thaigo
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/358411
- investigate: https://gitlab.com/gitlab-org/gitlab/-/issues/370169
- fix GDK
- update: https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/2593
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/121009
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-05-22
- update MacOS
- read: https://en.wikipedia.org/wiki/Security_Assertion_Markup_Language
- investigate: https://gitlab.com/gitlab-org/gitlab/-/issues/370169
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/118690

# 2023-05-17
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/409421
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/356791#note_1394458419
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/411675
- create demo for compliance framework export

# 2023-05-15
- post status updates on in progress isssues
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/404645
- fix GDK (postgres update)
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/356791
- coffee chat: Jan Kunzmann
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-05-12
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/120137
- start: secure code training
- read: upcoming kickoff/AMA slides [details kept SAFE]
- process emails
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/120636
- struggle with GDK (postgresql update)

# 2023-05-11
- process emails
- pairing: w/ Jay
- plan: https://gitlab.com/gitlab-org/gitlab/-/issues/356791
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/387912
- update: https://gitlab.com/gitlab-com/gl-security/security-department-meta/-/issues/1580

# 2023-05-10
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/105#note_1384465286
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/407162#note_1384953344
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/114991
- plan: https://gitlab.com/gitlab-org/gitlab/-/issues/356790

# 2023-05-09
- meeting: 1:1 w/ Nate
- read: essentialism for 15 min
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/100#note_1381159127
- process slack
- read: https://world.hey.com/dhh/even-amazon-can-t-make-sense-of-serverless-or-microservices-59625580
- meeting: Compliance Bi-Weekly
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/115157
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/393848
- plan: https://gitlab.com/gitlab-org/gitlab/-/issues/356790

# 2023-05-08
- read: essentialism for 15 min
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/389487#note_1375482988
- create: https://gitlab.com/gitlab-org/gitlab/-/issues/410148
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/119968
- process MR feedback: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/119783
- plan: https://gitlab.com/gitlab-org/gitlab/-/issues/356790

# 2023-05-05
- file expenses
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/119408
- install OS update
- read: essentialism for 15 min
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/118906#note_1379702078
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/114991/
- update: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/104#note_1377570932
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/119783

# 2023-05-04
- read: Compliance Weekly agenda
- fix: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/117471
- process feedback: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/118373
- pairing: w/ Jay on his implementatio plan for filters and then my MR !118373
- process email
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/119408

# 2023-05-03
- process slack
- update: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/123688
- process feedback: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/118373
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/404404#note_1376746169
- update: https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/2593

# 2023-05-02
- meeting: 1:1 w/ Nate
- https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/123688
- update: https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/2593
- coffee chat: Łukasz Korbasiewicz
- process slack
- read: https://gitlab.com/groups/gitlab-org/-/epics/10200
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/387912
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/118373

# 2023-04-28
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/117471
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/118612
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/408156#note_1371834069
- create: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/123688
- update: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/98

# 2023-04-27
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/118119
- update: https://gitlab.com/gitlab-com/gl-infra/production/-/issues/8655#note_1370118708
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/118906
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/407823#note_1360562184
- update: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/98#proposed-solution-summary-from-comment-thread-as-of-2023-04-27
- re-read: https://docs.gitlab.com/ee/development/sidekiq/worker_attributes.html#declaring-a-job-as-cpu-bound
- re-read: https://docs.gitlab.com/ee/development/sidekiq/worker_attributes.html#trading-immediacy-for-reduced-primary-load
- fix: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/117471

# 2023-04-26
- meeting: 1:1 w/ Nate
- meeting: 1:1 w/ Gayle
- meeting: Kaffeekraenzchen
- fixng: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/117471

# 2023-04-25
- prepare 1:1 agenda
- meeting: Compliance Weekly
- process MR feedback on https://gitlab.com/gitlab-org/gitlab/-/merge_requests/117471

# 2023-04-24
- file expenses
- process email
- read: https://about.gitlab.com/handbook/engineering/development/sec/govern/compliance/#demos
- read: https://about.gitlab.com/company/team/structure/#single-engineer-groups
- read: https://docs.gitlab.com/ee/user/group/compliance_frameworks.html#compliance-pipelines
- read: https://docs.gitlab.com/ee/api/runners.html#runners-api
- read: https://docs.gitlab.com/runner/register/
- https://docs.gitlab.com/runner/executors/docker.html
- create demo for: https://gitlab.com/gitlab-org/quality/testcases/-/issues/4017#note_1365036655

# 2023-04-21
- fix GDK
- process slack
- read: AMA- State of AI in Development https://docs.google.com/document/d/1IndOdsUgU1tf98rpf0LWBfcITC5vAO3i6-Ofi8EgXfU
- fix: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/117471
- update: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/98#note_1361845900
- search handbook for SAFE bounty objects
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/387912
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/398212

# 2023-04-20
- process emails
- process feedback: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/117471
- fix GDK

# 2023-04-19
- read feedback on: https://gitlab.com/gitlab-org/gitlab/-/issues/388484#note_1352334985
- update carreer development plan
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/114991
- process emails
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/117471

# 2023-04-18
- update carreer development plan
- coffee chat: Chiemi Sekizuka
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/388484#note_1356586438
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/28973#note_1357357548
- create: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/117894
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/384338
- report production issue on is-this-known with relevant logs
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/406585
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/404514
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/387911

# 2023-04-13
- read: http://rspec.info/blog/2016/07/rspec-3-5-has-been-released/
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/394630
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/387911
- create: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/98
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/407162#note_1351257603
- read: https://docs.gitlab.com/ee/development/testing_guide/testing_levels.html#consider-not-writing-a-system-test
- meeting: 1:1 w/ Alex Hansellka
- meeting: 1:1 w/ Jay
- fix: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/117471
- fix GDK

# 2023-04-12
- process email
- fullfill workday compliance tasks
- process slack
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/388686
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/374110#note_1330946090

# 2023-04-06
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/388686
- read: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/122556
- add appointment schedules to Google Calendar
- update: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/122556#note_1342996319

# 2023-04-05
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/388686
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/398150
- meeting: Sec Section Coffee Chat (AMER/EMEA)

# 2023-04-04
- meeting: 1:1 w/ Nate
- process slack
- update: https://gitlab.com/groups/gitlab-org/-/epics/8572
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/402388#note_1340432268
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/116541
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/89#note_1338147228
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/388686

# 2023-04-03
- read: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/116327
- read: https://about.gitlab.com/company/team/structure/working-groups/ai-integration/
- read: https://gitlab.com/groups/gitlab-org/-/epics/5925
- upload: https://www.youtube.com/watch?v=Q52GnRlO4HM
- update: https://gitlab.com/groups/gitlab-org/-/epics/6188
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/388686

# 2023-03-31
- process emails
- read: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/122063
- process slack
- write lambda function to store audits to AWS S3
- create demo video for storing streamed audit events on S3
- create: https://gitlab.com/-/snippets/2522467

# 2023-03-30
- meet up in Amsterdam
- travel homne

# 2023-03-29
- meet up in Amsterdam

# 2023-03-28
- meeting: 1:1 w/ Nate
- meeting: Compliance Weekly
- travel to Amsterdam
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/388686

# 2023-03-27
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/115537
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/115527
- plan logistics for Amsterdam meetup
- create: https://gitlab.com/gitlab-org/gitlab/-/issues/402388

# 2023-03-23
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/397718
- process slack
- read: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/115404
- process emails
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/393119#note_1324935429
- https://gitlab.com/gitlab-org/gitlab/-/issues/388686

# 2023-03-22
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/115144
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/374112#note_1322089651
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/393446#note_1321260528
- https://gitlab.com/gitlab-org/gitlab/-/issues/397718
  - check for post deployment index creation
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/379195

# 2023-03-21
- meeting: 1:1 w/ Nate
- process slack
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/111312
- read: https://docs.gitlab.com/ee/development/reusing_abstractions.html#service-classes
- create: https://gitlab.com/gitlab-org/gitlab/-/issues/398150
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/115221

# 2023-03-20
- process slack
- read: https://gitlab-com.gitlab.io/gl-infra/platform/stage-groups-index/compliance.html
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/89#note_1320036690
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/397027
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/114991
- update: https://gitlab.com/gl-retrospectives/govern/compliance/-/issues/33
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/1152212

# 2023-03-17
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/111312
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/114600

# 2023-03-16
- https://gitlab.com/gitlab-org/gitlab/-/issues/374110
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/114888
- meeting: 1:1 w/ Micheal
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/327909#note_1317328209
- meeting: Post Earnings Assembly
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/393446
  - Need more information to complete
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/114600
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/111312

# 2023-03-15
- process slack
- fix local dev env
- https://gitlab.com/gitlab-org/gitlab/-/issues/374110

# 2023-03-14
- meeting: 1:1 w/ Nate
- meeting: Compliance Weekly
- read: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/121348#note_1312508794
- process emails
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/111312
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/391295#note_1312201170
- read: updates on https://gitlab.com/gitlab-com/people-group/compliance/-/issues/187
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/391419#note_1309316909
- process slack
- watch: FY23Q4 Earnings Call https://www.youtube.com/watch?v=OPnSehYpxKs
- fill out carreer development

# 2023-03-13
- set up CalDigit TS4
- process emails
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/395530#note_1308996553
- read: https://gitlab.com/gitlab-com/people-group/compliance/-/issues/187
- proces slack
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/111312

# 2023-03-10
- https://gitlab.com/gitlab-org/gitlab/-/issues/263199

# 2023-03-09
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/395530#note_1306916104
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/113453
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/114099
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/111312
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/113156

# 2023-03-08
- https://gitlab.com/gitlab-org/gitlab/-/issues/374110
- update: https://gitlab.com/gitlab-com/marketing/brand-product-marketing/brand-strategy/-/issues/11
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/113453
- fix gpg setup
  brew install pinentry-mac && killall gpg-agent
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/374109

# 2023-03-07
- meeting: 1:1 w/ Nate
- read: https://gitlab.com/gitlab-com/anti-abuse-govern-growth-sub-dept/-/issues/50
- watch: Compliance Weekly 2023-03-07 recording
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/113453
- format Compliance Weekly agenda with GitLab corporate design elements
- take resilience course on LevelUp & LinkedIn Learning
  https://www.linkedin.com/learning/certificates/398ee3287d6919e81ffac13d1d7ae5c2bcfec74b9f33aa73a77c5ae14ad57f4f
- coffee chat: Laurent Soyris
- coffee chat: Cassiana Gudgenov
- watch: https://www.youtube.com/watch?v=w_CRpuPwO0o
- meeting: ClickHouse Training Discussion w/ Gitlab
- update: https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/14384
- update: https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/14462#note_1302895647
- setup gpg

# 2023-03-06
- process emails
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/80
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/82
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/394630
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/392415
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/113453
- watch: https://www.youtube.com/watch?v=498qjN4PwS8
- update: https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/14462

# 2023-03-03
- forgot to log work today

# 2023-03-02
- process emails
- https://gitlab.com/gitlab-org/gitlab/-/issues/391295
- coffee chat: Chamellia Yang
- coffee chat: Sean John Hoyle
- meeting: 1:1 w/ Kamil
- coffee chat: Jay Montal
- https://gitlab.com/gitlab-org/gitlab/-/issues/374109

# 2023-03-01
- process emails
- read: https://gitlab.com/gitlab-org/quality/triage-ops/-/merge_requests/2007
- update: https://gitlab.com/gitlab-com/Product/-/issues/5468
- read: expense policy clarification on slack
  spent way too much time going over discussions and MRs on this
- check error budget for Compliance
- search for staging-ref's prometheus location
  turns out we don't have it setup properly so you can only access it with cluster access and port forwarding
- experiment with Thanos and prometheus queries to learn wildcards
- https://gitlab.com/gitlab-org/gitlab/-/issues/391295
  - activate feature flag globally for staging-ref
  - monitor workers in Thanos

# 2023-02-28
- coffee chat: Huzaifa Iftikhar
- meeting: 1:1 w/ Nate
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/82
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/112940
- read: https://docs.gitlab.com/ee/user/group/compliance_frameworks.html#example-configuration
- read: https://docs.gitlab.com/ee/user/compliance/compliance_report/index.html
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/113070
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/367549
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/345514#note_1291770681
- install teleport CLI
- watch: https://youtu.be/yyEn_FDkE1g
- meeting: 1:1 w/ Kamil
- https://gitlab.com/gitlab-org/gitlab/-/issues/391295
  Working out strategy to test feature with limited rollout
- read: https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/endpoint-management/jamf/
- read: https://about.gitlab.com/handbook/it/
- schedule laptop wipe
- get old laptop wiped

# 2023-02-24
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/111312
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/21916
- watch: https://levelup.gitlab.com/learn/video/fy-24-q1-quarterly-kickoff
- investigate: https://gitlab.com/gitlab-org/gitlab/-/issues/345514#note_1287109739
- read: https://about.gitlab.com/direction/govern/compliance/
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/blob/main/FE.md
- https://gitlab.com/gitlab-org/govern/compliance/general/-/merge_requests/18
- read: https://docs.gitlab.com/ee/administration/audit_reports.html
- read: https://about.gitlab.com/direction/govern/compliance/audit-events/
- read: https://docs.gitlab.com/ee/user/group/compliance_frameworks.html#compliance-frameworks

# 2023-02-23
- read: https://gitlab.com/groups/gitlab-org/-/epics/8383
- watch: https://www.youtube.com/watch?v=ckJkNumH5sM
- read: https://about.gitlab.com/handbook/engineering/development/sec/govern/compliance/
- process email
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/375545
- read: https://clickhouse.com/docs/en/intro/
- read audit events related code in backend
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/111312

# 2023-02-22
- fix GKD
  - Authentiq and webpack errors were to blame
- https://gitlab.com/gitlab-org/gitlab/-/issues/391830
  - Check for index creation, seems it's still scheduled
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/111312

# 2023-02-21
- process email
- read: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/120086
- meeting: 1:1 w/ Nate
- coffee chat: Hitesh Raghuvanshi
- coffee chat: Michael Becker
- coffee chat: waited for Aaron Huntsman, no show
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/111312
- fix GKD

# 2023-02-20
- process emails
- read: Compliance Weekly 2023 agenda
- read: https://gitlab.com/gitlab-com/Product/-/issues/5468
- schedule coffee chats
- https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/76 Compliance Onboarding Issue
- read: https://about.gitlab.com/handbook/product/product-principles/#sts=SaaS%20First
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/80#note_1284673968
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/74
- read: https://about.gitlab.com/handbook/engineering/demos/#single-engineer-groups-demo
- read: https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/122975580
- read: https://docs.gitlab.com/ee/administration/audit_events.html
- read: https://docs.gitlab.com/ee/administration/audit_event_streaming.html
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/112446
- https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/120121
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/72
- read: https://docs.gitlab.com/ee/development/audit_event_guide

# 2023-02-17
- process emails
- meeting: SEC AMA (EMEA)
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/111312

# 2023-02-16
- meeting: 1:1 w/ Kamil & Nate
- meeting: coffee chat w/ Doug
- meeting: Growth Team sync
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/111312
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/111819

# 2023-02-15
- read: https://about.gitlab.com/direction/govern/compliance/compliance-management/
- read: https://gitlab.com/groups/gitlab-org/-/epics/2537
- read: https://about.gitlab.com/handbook/product/personas/#cameron-compliance-manager
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/104415
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/111962

# 2023-02-14
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/111962

# 2023-02-13
- https://gitlab.com/gitlab-org/gitlab/-/issues/391296

# 2023-02-10
- https://gitlab.com/gitlab-org/gitlab/-/issues/391295
- https://gitlab.com/gitlab-org/gitlab/-/issues/391296

# 2023-02-09
- https://gitlab.com/gitlab-org/gitlab/-/issues/379195
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/98438
- meeting: 1:1 w/ Kamil
- pairing w/ Doug on #379195
- collaborate on https://gitlab.com/gitlab-org/gitlab/-/issues/390637#note_1272980023

# 2023-02-08
- https://gitlab.com/gitlab-org/gitlab/-/issues/379195
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/98438

# 2023-02-07
- https://gitlab.com/gitlab-org/gitlab/-/issues/379195
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/98438

# 2023-02-06
- https://gitlab.com/gitlab-org/gitlab/-/issues/379195
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/98438

# 2023-02-03
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/98438

# 2023-02-02
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/109596
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/98438
- meeting: 1:1 w/ Kamil
- meeting: Walkthrough TriageOps w/ Roy Liu

# 2023-02-01
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/109596
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/98438
- https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/677#note_1261207946
- process email

# 2023-01-31
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/98438
- process email
- refinements
- https://gitlab.com/gitlab-org/quality/triage-ops/-/merge_requests/1930
- https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/677#note_1259232639

# 2023-01-30
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/109645
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/98438
- refinements
- finish mandatory anual legal compliance trainings
- process email

# 2023-01-26
- meeting: 1:1 w/ Kamil
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/109645

# 2023-01-25
- https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/113300#note_1252704007
- https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/117600#note_1252690670
- https://gitlab.com/gitlab-org/gitlab/-/issues/389040
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/108989#note_1252595200
- meeting: Kaffeekraenzchen

# 2023-01-24
- fix local CustomersDot
- https://gitlab.com/gitlab-org/gitlab/-/issues/389040
- fix GDK
- https://gitlab.com/gitlab-org/gitlab/-/issues/384445
- https://gitlab.com/gitlab-org/gitlab/-/issues/358388

# 2023-01-23
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/108989

# 2023-01-20
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/109077
- coffee chat: Andrea Obermeier

# 2023-01-19
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/108989
- meeting: 1:1 w/ Kamil
- meeting: 1:1 w/ Gayle
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/109077

# 2023-01-18
- watch: Pods AMA intro and read slides
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/108989
- collaborate on: https://gitlab.com/gitlab-org/gitlab/-/issues/345514

# 2023-01-17
- investigate verify marketing mails' URLs
- tweak vim setup
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/108353
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/108989

# 2023-01-16
- Note: I seem to have forgotten that this log exists and have negelcted it the past few days.
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/107849
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/358080
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/108508
- https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/675
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/108989

# 2023-01-03
- tweak vim setup
- #367549
  - !95865

# 2023-01-02
- #367549
  - !95865
- Note: Stomach pain made it impossible to focus, had to cut the day short

# 2022-12-19
- Note: forgot to track activity here the past days, expect a gap.
- read: https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/16936#note_1200669276
- read: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/107000#note_1211124891
- https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/113300
- #367549
  - !95865

# 2022-12-13
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/106880
- collaborate on: https://gitlab.com/gitlab-org/gitlab/-/issues/381615#note_1208106910
- #367549
  - !95865
- Note: Having a slow day. I don't feel fully recovered yet.

# 2022-12-10
- https://gitlab.com/gitlab-org/gitlab/-/issues/375629
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/384527
- #367549
  - !95865

# 2022-12-09
- meeting: 1:1 w/ Kamil, sync about FUP(free user priority) issues
- #367549
  - !95865
- meeting: Sec, Growth, Data Science end of year catchup
- investigate bug in danger bot when parsing for feature_category
- re-read: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/104787

# 2022-12-07
- read: https://gitlab.com/gitlab-com/gl-infra/production/-/issues/8131
- #367549
  - !95865
- #384313
- read: Sentinal One news

# 2022-12-06
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/101575
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/105868

# 2022-12-05
- https://gitlab.com/gitlab-org/gitlab/-/issues/358388
- #367549
  - !95865
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/14086
  - And took a look at the code to try to understand how we render these notes

# 2022-12-02
- fix GDK
- https://gitlab.com/gitlab-org/gitlab/-/issues/369991

# 2022-12-01
- meeting: 1:1 w/ Kamil
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/105384
  - process review feedback
- coffee chat: waited for Jennifer Schmidt, no show
- https://gitlab.com/gitlab-org/gitlab/-/issues/379109

# 2022-11-30
- fix GDK
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/105485
- pair program w/ Doug on membership count query optimizations
- meeting: Kaffeekraenzchen

# 2022-11-29
- https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/3164
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/101575
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/105247
  - Investigating flaky CI specs
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/105384

# 2022-11-28
- process email
- read: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/104787
- fix GDK
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/102317
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/105193

# 2022-11-23
- process email
  #367549
  - !95865
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/104859

# 2022-11-22
- #367549
  - !95865
- investigate PTO by Roots API
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/102317
- #375629
- pair program w/ Doug on #383264

# 2022-11-21
- engineer on call shift
- review: !104358
- collaborate on: https://gitlab.com/gitlab-org/gitlab/-/issues/371197
- #375629
  - research data on root cause

# 2022-11-17
- collaborate on: https://gitlab.com/groups/gitlab-org/quality/contributor-success/-/epics/1
- meeting: 1:1 w/ Kamil
- watch: FY23-Q4 Development Quarterly Kickoff Pre-video
- #375629
- read docs: https://github.com/settingslogic/settingslogic
- #367549
  - !104415

# 2022-11-16
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/372453
- read: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/91919
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/104070
- watch: FY23-Q4 Kickoff
- search issues related to alert refactoring
- #367549
  - !95865
  - !103034

# 2022-11-16
- #375629
- meetinng: 1:1 w/ Doug, discussing #375629
- #382563
  - research namespace traversal landscape
  - test query performance with acquired data
- meeting: 1:1 w/ Doug, discussing `over_limit?` improvement

# 2022-11-14
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/103238
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/103375
- #375629
- watch: Recording of vue3-migration-status-update-2022-11-09
- read: FY23Q3 SUS Results SaaS and SM - https://docs.google.com/spreadsheets/d/17weEAe-WaLZ5xAM32wpcN8KHBkzNrUyvJCtsLPnZd8E/edit#gid=127824580
- enable chat ops for Ross

# 2022-11-11
- #375629

# 2022-11-10
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/95829
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/103238
- meeting: 1:1 w/ Kamil
- Went on a tangent https://gitlab.com/gitlab-org/gitlab/-/issues/376388#note_1168194244
- #367549
  - !103034

# 2022-11-09
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/103238
- review: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/114615
- #367549
  - !95865
- collaborate on: https://gitlab.com/gitlab-org/gitlab/-/issues/323226
- #375629
- meeting: Growth Acquisition Team Meeting

# 2022-11-08
- meeting: Acquisition EMEA skip level
- #367549
  - !95865
  - !103034
- meeting: Onboarding Buddy 1:1 with w/ Ross Byrne
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/102909
- https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/merge_requests/1965
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/103167
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/102797
- setup deno
- file expense report
- investigate how we display alerts for pipeline fetch failures, as they can pile up and cover the whole page contents

# 2022-11-07
- process emails
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/102797
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/102909
- #367549
  - !95865
  - !103034

# 2022-11-04
- collaborate on: https://gitlab.com/gitlab-org/gitlab/-/issues/378430#note_1159348965
- #367549
  - !95865
  - !103034
- #345514
  - !101575
- pair program w/ Jay on #345514 - short catch up session

# 2022-11-03
- tweak Okta setup
- finish talent assessment
- meeting: 1:1 w/ Kamil
- document Okta compliance tracking
- collaborate on: https://gitlab.com/gitlab-org/gitlab/-/issues/378430
- pair program w/ Jay on #345514

# 2022-11-02
- tweak Okta setup
- meeting: Onboarding Buddy 1:1 with w/ Ross Byrne
- coffee chat: Roy Liu
- https://gitlab.com/gitlab-com/dev-sub-department/section-dev-request-for-help/-/issues/19
- #367549
  - !95865
- meeting: Early Career Professionals TMDG
- pair program w/ Jay on #345514 - short catch up session

# 2022-10-31
- review: !102291
- collaborate on: NextUp issues refinement
- #367549
  - !95865
- start workday talent assessment
- meeting: 1:1 w/ Doug - free user cap - read only catch up

# 2022-10-28
- post update summary for #367549
- write: https://gitlab.com/gitlab-org/gitlab/-/issues/379195
- read: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/102260
- meeting: check in on #367549 w/ Kamil
- #367549
  - !95865
- record welcome video for Ross

# 2022-10-27
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/92878
- #367549
  - !95865
- read: https://docs.gitlab.com/ee/development/database/understanding_explain_plans.html
- read: https://www.postgresql.org/docs/current/sql-explain.html
- meeting: 1:1 w/ Kamil
- meeting: w/ Doug - rubber ducking read-only caching

# 2022-10-26
- process emails
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/92878
- process slack
- #367549
  - !95865
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/372196
- collaborate on: https://gitlab.com/gitlab-org/gitlab/-/issues/375748
- pair program w/ Jay on #345514
- meeting: Growth Acquisition Team Meeting

# 2022-10-25
- process emails
- process slack
- read: https://docs.gitlab.com/ee/development/database/creating_enums.html#creating-enums
- do talent assessment tasks
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/92878
- #367549
  - !95865

# 2022-10-24
- read: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/110967
- !92878
- coffee chat: waited for Jamie Maynard, no show
- #367549
  - !95865
- sign up for 2022-11 on-call schedule
- https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/113300

# 2022-10-21
- #345514
- read up on: https://gitlab.com/gitlab-org/ruby/gems/gitlab-experiment/-/blob/master/README.md
- read: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/101579
- file expenses
- review: !101354
- #367549
  - !95865
- review: !101564
- review: !101767
- read: https://gitlab.com/groups/gitlab-org/-/epics/8926
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/376871

# 2022-10-20
- process emails
- meeting: 1:1 w/ Kamil
- #345514
- meeting: Doug / Sam #367549
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/378519
- #367549
  - !95865

# 2022-10-19
- process emails
- https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/113300
- #367549
  - !95865
- read: https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/architecture/blueprints/pods/index.md
- pair program w/ Jay on #345514

# 2022-10-18
- https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/113300
- process emails
- #367549
  - !95865

# 2022-10-17
- review: !101026
- review: !101118
- !100460
- !100538
- collaborate on: https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/660#note_1131485232
- process emails
- process slack
- #367549
  - !95865
- https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/113300

# 2022-10-10
- collaborate on: https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/660
- #367549
  - !95865

# 2022-10-09
- engineer on call shift
- review: !100331

# 2022-10-06
- #367549
  - !100311
- meeting: 1:1 w/ Kamil
- read up on: MECC to TechOps rebranding
- !100460

# 2022-10-05
- collaborate on: #375726
- #367549
  - !95865
  - !100311

# 2022-10-04
- #367549
  - !95865
 - process slack
- read: https://docs.gitlab.com/ee/development/database/iterating_tables_in_batches.html
- review: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/112071

# 2022-09-30
- review: !99036
- review: !99027
- review: !98691

# 2022-09-29
- review: !99027
- meeting: 1:1 w/ Kamil
- coffee chat: James Rushford
- coffee chat: Rodrigo Rios

# 2022-09-28
- process emails
- #367549
  - !95865
  - !98438
- meeting: Kaffeekränzchen (GitLab DE social meeting)
- meeting: Growth Acquisition Team Meeting
- review: !99027

# 2022-09-27
- process slack
- read: FY’23 Q3 Development Offsite Readout
- read: https://about.gitlab.com/handbook/engineering/
- update email signature for German entity
- #367549
  - !95865

# 2022-09-21
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/373194
- read: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/97556
- #367549
  - !95865

# 2022-09-20
- process email
- #367549
  - !98438
  - !95865

# 2022-09-19
- #367549
- coffee chat: waited for Rodrigo Rios, no-show
- process slack

# 2022-09-16
- #367549
- take LinkedIn course: Think Creatively by Drew Boyd

# 2022-09-15
- meeting: 1:1 w/ Kamil
- meeting: Growth Acquisition Team Meeting
- #367549

# 2022-09-14
- read: https://docs.gitlab.com/ee/development/sidekiq/idempotent_jobs.html
- read: https://github.com/mperham/sidekiq/wiki/Best-Practices#2-make-your-job-idempotent-and-transactional
- review: https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/2777
- #367549
  - !95865

# 2022-09-13
- review: !97400
- process slack
- process emails
- #367549
  - !97517
  - !95865

# 2022-09-12
- review: !95150
- review: !96703
- !97517
- review: https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/2777
- #368999
- process emails

# 2022-09-09
- review: !96871
- #367549
- read: https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/postgresql.md
- read: https://docs.gitlab.com/ee/development/testing_guide/testing_migrations_guide.html
- read: https://docs.gitlab.com/ee/development/database/migrations_for_multiple_databases.html
- read: https://docs.gitlab.com/ee/development/sidekiq/logging.html#sidekiq-logging
- read: https://docs.gitlab.com/ee/development/changelog.html#changelog-entries
- !97517

# 2022-09-08
- meeting: 1:1 w/ Gayle
- process emails
- #367549

# 2022-09-07
- rotate personal access tokens
- !97039
- process emails
- read updates on #368999
- #372787
- review: !96871
- review: !97232
- #367549

# 2022-09-06
- process slack
- meeting: 1:1 w/ Kamil
- !97039
- #367549
- collaborate on: https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/6740#note_1088616517

# 2022-09-05
- continue: set up new laptop
- review: !96817
- review: !96859
- !97039
- #367549

# 2022-09-02
- read: https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/
- continue: set up new laptop

# 2022-09-01
- continue: set up new laptop
- coffee chat: Alex Hanselka

# 2022-08-31
- #367549
- set up new laptop
- coffee chat: Kaitlyn Chappell
- meeting: Kaffeekraenzchen
- meeting: Growth Acquisition Team Meeting

# 2022-08-30
- meeting: 1:1 w/ Kamil
- take LinkedIn course: Creativity at Work: A Short Course from Seth Godin
- take LinkedIn course: The Secret to Better Decisions: Stop Hoarding Chips
- #367549
- review: !95473

# 2022-08-26
- #367549
- meeting: virtual baby shower for Gayle

# 2022-08-25
- process email
- #367549
- process slack
- coffee chat: Anthony Natale
- meeting: Acquisition Sync

# 2022-08-24
- review: !94252 (follow-up)
- review: !95968
- #367549

# 2022-08-23
- #367549
- process email
- meeting: Doug / Sam Catchup Free User Cap
- review: !95812

# 2022-08-22
- process email
- review: !94252 (follow-up)
- read: https://docs.google.com/document/d/1jqDP5RxyezhyFn4dX4wDzZ-3awqoXlKLFjZaCgsVf68/edit#
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/371201
- #367549

# 2022-08-20
- engineer on call shift

# 2022-08-19
- #367549
- schedule PTO
- process slack
- meeting: Code Review Weekly Workshop

# 2022-08-18
- #367549
- process email
- !95365
- review: !94252 (follow-up)
- process slack

# 2022-08-17
- review: !94252
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/367489
- post async stand up
- !95365
- meeting: 1:1 w/ Gayle
- meeting: Growth Acquisition Sync

# 2022-08-16
- meeting: Acquisition EMEA skip level
- process slack
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/370616
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/370784
- read: https://gitlab.com/gitlab-org/error-budget-reports/-/issues/11
- read: https://gitlab.com/groups/gitlab-org/-/epics/8580
- process email
- meeting: German Payroll AMA
- file expenses
- !95365
- review: !94252

# 2022-08-15
- process email
- #368999
- #368529
- process slack
- read: Engineering Week-in-Review notes
- read: https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/13311
- review: !94252
- #367549

# 2022-08-12
- process email
- review: !93791
- review: !95216
- process slack
- meeting: Code Review Weekly Workshop

# 2022-08-11
- #367549
- process email

# 2022-08-10
- #325399
- review: !95027
- review: !94983
- process email
- process slack

# 2022-08-09
- process email
- #368999
- #325399
- coffee chat: Doug Stull
- coffee chat: Sophia Manicor

# 2022-08-08
- read: Engineering Week-in-Review notes
- read followup on !92762
- process emails
- process slack
- watch: recording of 2022-08-03 Acquisition team sync
- complete MECC course on Level Up

# 2022-07-22
- watch: AMA with Sid https://www.youtube.com/watch?v=3SLYfxV7aTk
- !93020
- !92762
- coordinate coverage
- prepare coverage issue

# 2022-07-21
- process slack
- process emails
- test my merged issue mentioned in QA https://gitlab.com/gitlab-org/release/tasks/-/issues/4160
- #368418
- !93020
- 368529

# 2022-07-20
- process emails
- process slack
- !92878
- meeting: 1:1 w/ Gayle
- coffee chat: Erin Barr
- !92762
- review: !92891
- #368418

# 2022-07-19
- #325525
- collaborate on: https://gitlab.com/gitlab-org/gitlab/-/issues/365353
- collaborate on: https://gitlab.com/gitlab-org/gitlab/-/issues/367549
- review: !92658
- process emails

# 2022-07-18
- process emails
- process slack
- https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/107700
- #325525
- coffee chat: Constantin Pahl
- set feature flags on staging

# 2022-07-15
- process slack
- read: https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/2478
- read: https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/post_deploy_migration/readme.md
- read: https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/585
- https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/4150
- #325525

# 2022-07-14
- process emails
- follow up on EMEA laptop refresh program
- process slack
- fill: Mid-Year Check-In
- https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/4150

# 2022-07-13
- process slack
- meeting: 1:1 w/ Kamil
- post async stand up
- process emails
- collaborate on: https://gitlab.com/groups/gitlab-org/-/epics/8153
- review: !92223
- complete annual security training: Anti-Phishing Education
- meeting: Growth Acquisition Sync
- file expenses

# 2022-07-12
- process slack
- sign up for 2022-08 on-call schedule
- update GDK
- fill EMEA laptop refresh survey
- process email
- read: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/107060
- collaborate on: #365673
- fill out IGP 6 Month Reflection Point
- https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/4150
- coffee chat: Gayle

# 2022-06-30
- watch: Acquisition Team Sync - 2022-06-29
  - https://www.youtube.com/watch?v=r09knDX7QfI
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/365673
  - read related: comms rollout sheet
- review: !91062
- meeting: Engineering Sync: Preview Strategy

# 2022-06-28
- prepare 1:1 agenda
- meeting: 1:1 w/ Kamil
- https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/4150

# 2022-06-27
- process slack
- post async stand up
- read: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/106408
- https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/4150
- #362104
  - #note_1002650981
- review: !90484

# 2022-06-23
- post async stand up
- process slack
- collaborate on: #355034
- collaborate on: #365844
- https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/4150
- process emails
- #362104
  - #note_1002650981

# 2022-06-22
- https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/4150
- process slack
- setup workday
- meeting: 1:1 w/ Gayle
- coffee chat: Jay Montal
- process emails
- coffee chat: Dallas Reedly

# 2022-06-21
- read: #26664
- read: #364312
- process emails
- prepare 1:1 agenda
- meeting: 1:1 w/ Kamil

# 2022-06-20
- re-review: !85359
- post async stand up
- process emails
- !90084
- try to sort out ESPP woes
- process slack
- help Robert on !90135

# 2022-06-15
- review: !85359
- !90084
- process emails
- read: https://gitlab.com/gitlab-org/frontend/rfcs/-/issues/102
- coffee chat: Jason Shippee
- meeting: Growth Conversion Team Meeting

# 2022-06-14
- review: !89975
- review: !85359
- process emails
- #362104
- process slack
- read: https://about.gitlab.com/handbook/engineering/development/growth/acquisition/
- #363995

# 2022-06-13
- read: A/B Testing Intuition Busters
  https://drive.google.com/file/d/1oK2HpKKXeQLX6gQeQpfEaCGZtNr2kR76/view
- #363995
- !90084
- !89768

# 2022-06-10
- prepare for Hobby Corner session
- meeting: GrwothDay activities, Hobby Corners and scribbl.io
- #364017
- !89768
- #363995

# 2022-06-09
- process emails
- process slack
- post async stand up
- shuffle calendar schedule
- #325525
- #362104
- read: https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/6450
- read: &4974
- review: !88759
- write outline for hobby sharing session

# 2022-06-08
- fix local snowplow-micro setup
- post async stand up
- !86684
- #362104
- read: Engineering Week-in-Review notes
- fill out maintainer/reviewer survey
- read: https://about.gitlab.com/handbook/product/product-intelligence-guide/getting-started/
- #325525
- file expenses
- process slack
- scope out potential pajamas day issues

# 2022-06-07
- process email
- #325525
- meeting: 1:1 w/ Kamil
- handle ESPP accounts
- process slack
- fight local snowplow-micro setup
- meeting: Managing Your Account for Non-US Participants

# 2022-06-03
- process slack
- deal with JAMF alert
- process email
- #325525
- watch: Myths, Facts, and the Future:  Working on the Authentication and Authorization Team at GitLab

# 2022-06-02
- create referral
- #362104
- review: !88759
- take LinkedIn course: Career Wellness Nano
- read: https://www.linkedin.com/pulse/remote-vs-what-whatin-short-read-mark-doobles-deubel/
- !86684
- take LinkedIn course: Managing Stress
- review: !88901
- review: !89027
- https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/4150

# 2022-06-01
- coffee chat: Alessandra Seidita
- !86684
- process slack
- https://gitlab.com/gitlab-com/gl-infra/production/-/issues/7129
  - add https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/105247
- process email
- review: !88901
- review: !89027
- meeting: Growth Conversion Sync

# 2022-05-31
- !86684
- review: !85222
- https://gitlab.com/gitlab-com/gl-infra/production/-/issues/7129
- prepare 1:1 agenda
- meeting: 1:1 w/ Kamil
- process slack
- process email
- meeting: 1:1 Stephan E.

# 2022-05-30
- #360017
- process email
- process slack
- review: !88819
- !86684

# 2022-05-25
- forgot to log today

# 2022-05-24
- prepare 1:1 agenda
- meeting: 1:1 w/ Kamil
- update macOS
- #360017
- https://gitlab.com/gitlab-com/gl-infra/production/-/issues/7129
- #350344

# 2022-05-23
- fighting GDK
- #362103
- #362102
- #362104
- #360017

# 2022-05-20
- read: https://gitlab.com/gitlab-com/gl-infra/production/-/issues/7096
- #360017

# 2022-05-19
- #360017
- read: https://gitlab.com/cwoolley-gitlab/cwoolley-gitlab/-/blob/main/gitlab-workstation-setup-notes.md
- meeting: AMA Marketing & R&D
- pinged on incident 7099

# 2022-05-18
- #362104
- meeting: 1:1 w/ Kamil
- meeting: Growth Conversion Team Meeting
- #360017

# 2022-05-17
- process email
- fill out Culture Amp survey
- remove Clockwise
  - it was giving me more grief than utility
- review: !87569
- investigate broken master
  - https://gitlab.com/gitlab-org/gitlab/-/jobs/2465960663
- prepare 1:1 agenda for tomorrow

# 2022-05-16
- review: !85222
- review: !87557
- review: !87569

# 2022-05-13
- !87288 fix merge conflict
- collaborate on https://gitlab.com/gitlab-org/gitlab/-/issues/362104
- !87304
- process slack
- process email
- #360017
- meeting: 1:1 w/ Doug about #362104

# 2022-05-12
- note: somehow forgot to log activities today, my mind was all over the place

# 2022-05-11
- review: !87199
- do the GDK update thing
- process emails
- process slack
- #360017

# 2022-05-10
- meeting: 1:1 w/ Kamil
- process emails
- !84968
- #360017

# 2022-05-09
- #360017
- coffee chat: Ryan Tucker

# 2022-05-06
- process slack
- sirt-2266
- #360017

# 2022-05-05
- collaborate on https://gitlab.com/groups/gitlab-org/-/epics/4974
- process slack
- process email
- #360017

# 2022-05-04
- #349817
- pair program w/ Nicolas on #349817
- coffee chat: Kaitlyn Chappell
- read: https://gitlab.com/groups/gitlab-org/-/epics/4974
- meeting: Growth Conversion Team Meeting

# 2022-05-03
- #349817
- process slack
- meeting: 1:1 w/ Wayne, Shadow program follow-up
- process emails

# 2022-05-02
- #349817
  - having disagreements with undercoverage reports
- process slack
- meeting: 1:1 w/ Wayne, shadow program follow-up
- fight kernel panics

# 2022-04-29
- process slack
- #349817

# 2022-04-28
- fight kernel panics
- process slack
- process emails
- fill out section 2 of Individual Growth Plan
- read: PA analysis https://gitlab.com/gitlab-data/product-analytics/-/issues/400
- read: PA analysis https://gitlab.com/gitlab-data/product-analytics/-/issues/416
- meeting: Doug / Sam Catchup Free User Cap
- shadowing: Wayne & Neil
- shadowing: Performance Indicator Meeting - Sec
- meeting: Secure::Static Analysis Office Hours
- shadowing: Wayne & David G.

# 2022-04-27
- coffee chat: MJ Kwack
- fill out section 1 of Individual Growth Plan
- meeting: Kaffeekraenzchen
- shadowing: John/Wayne review current dashboards/processes
- feedback on Flow Framework application for working group next prio
- shadowing: Sec Growth ModelOps staff meeting
- shadowing: Wayne & Mon
- collaborating on https://gitlab.com/gitlab-org/gitlab/-/issues/360694

# 2022-04-26
- read: Flow Framework
  - https://danlebrero.com/2021/02/10/project-to-product-flow-framework-summary/
  - https://www.tobysinclair.com/post/book-summary-project-to-product-by-mik-kersten-flow-framework
- meeting: 1:1 w/ Kamil
- shadowing: Maintainence First <> SIGs sync
- shadowing: Wayne & Thomas G.
- meeting: Engineering Allocation
- shadowing: Wayne & Hillary
- shadowing: Wayne & David

# 2022-04-25
- process slack
- read: issues related to &7757
- read: https://about.gitlab.com/company/team/structure/working-groups/maint-first/ and related slack channel
- read: Development Staff Meeting 2022 agenda
- meeting: Conversion Engineering Free User Cap discussion
- meeting: 1:1 w/ Wayne
- meeting: 1:1 w/ Doug
- shadowing: Wayne & Michael L.'s mentoring
- #349817

# 2022-04-14
- read: docs Migrations for Multiple databases
  - https://docs.gitlab.com/ee/development/database/migrations_for_multiple_databases.html
- #349817

# 2022-04-13
- #349817
- process slack
- coffee chat: Ross Fuhrman
- meeting: Product Analysis Office Hours

# 2022-04-12
- process slack
- #349817
- meeting: 1:1 w/ Kamil
- process emails
- read: docs for declarative-policy

# 2022-04-09
- engineer on call shift

# 2022-04-08
- process emails
- review !84188
- !84485
- write #358388
- review !84367
- process slack
- #263199

# 2022-04-07
- !84485
- process emails
- process slack
- read: https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/3659
- review !84367
- meeting: Growth Social Hour
- meeting: Interview about instrumentation w/ Amanda

# 2022-04-06
- process emails
- collaborate on: https://gitlab.com/gitlab-org/gitlab/-/issues/353148#note_901667437
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/357904
- read: Growth Sub-department Engineering Weekly minutes
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/357911
- meeting: Doug about Free User Cap
- #263171
- #349817

# 2022-04-05
- coffee chat: Hinam Mehra
- meeting: 1:1 w/ Kamil
- meeting: 1:1 w/ Phil
- meeting: Free user cap engineering discussion
- meeting: Growth Weekly
- read: NamespaceSetting related code paths

# 2022-04-04
- get local customerDot to comply
- !83979
- process emails
- watch: Testing ML Models for production https://www.youtube.com/watch?v=KJKEaJ88Aho
- process slack
- review !84188

# 2022-03-31
- !83979
- !82738
- process emails
- process slack
- 1/2 day PTO

# 2022-03-30
- process emails
- clear EoC schedule ping
- review !83156
- meeting: Growth Sub-department Engineering Weekly
- pair program w/ Nicolas
- process slack
- !83979
- fight local CustomerDot setup

# 2022-03-29
- process emails
- meeting: 1:1 w/ Kamil
- add and augment conventional comments workflow to Alfred app
  - Link: https://github.com/krzysztofzuraw/alfred-conventional-comments
- review !83476
- review !83156

# 2022-03-28
- process emails
- pick EoC shift for April
- review !83714
- process slack
- read: Engineering Week-in-Review notes
- !82738
- #349817

# 2022-03-25
- Family & Friends Day

# 2022-03-24
- finsih review !82526
  - seems my GDK/CustomerDot is not workign properly couldn't test locally and fiddled with the setup a lot
- #349817
- !82738

# 2022-03-23
- process emails
- read: %14.9 growth retrospective contributions
- continue feature flag training
- review !82526
- #349817

# 2022-03-22
- coffee chat: Danny L'Estrange
- meeting: 1:1 w/ Kamil
- review: !83387
- #349817
- meeting: Growth Weekly

# 2022-03-21
- process slack
- process emails
- write retro notes
- !82738

# 2022-03-18
- process emails
- process slack
- help Matej on #356255
- !82738

# 2022-03-17
- process emails
- process slack
- pinged on: https://gitlab.com/gitlab-com/gl-infra/production/-/issues/6623
- collaborate on: https://gitlab.com/gitlab-data/product-analytics/-/issues/158#note_877459119
- !82738
- meeting: Wayne Haber AMA

# 2022-03-16
- process emails
- process slack
- !82738
- update nvim setup w/ 'ruanyl/vim-gh-line'
- meeting: Change Network: Team Member Experience
- meeting: Conversion 14.10 Kickoff

# 2022-03-15
- process emails
- process slack
- meeting: 1:1 w/ Kamil
- write status report for ongoing issues
- meeting: 1:1 w/ Wayne, skip-level
- review https://www.youtube.com/watch?v=xC9cOZkIhp0 and give feedback
- process slack
- !82738
- apply review for !80906

# 2022-03-13
- engineer on call shift

# 2022-03-11
- collaborate on: https://gitlab.com/gitlab-org/gitlab/-/issues/355527
- review !82636
- #352629
- meeting: 2:1 Neil & Gayle

# 2022-03-10
- #352629
- coffee chat: Gayle Goud
- meeting: Speakers Lean Coffee

# 2022-03-09
- continue to read: Hands-on Machine Learning (chapter 2)
- process emails
- review !82278
- review !82448
- collaborate on: https://gitlab.com/gitlab-data/product-analytics/-/issues/158#note_868403060
- collaborate on: https://gitlab.com/gitlab-org/gitlab/-/issues/342853#note_867715650
- #352629
- meeting: Growth Conversion Sync

# 2022-03-03 - 2022-03-09
- totally neglected logging due to existential dread due to world events

# 2022-03-02
- process slack
- process emails
- tweak local setup: CustomerDot not working

# 2022-03-01
- meeting: 1:1 w/ Kamil
- process emails
- process slack
- sign up for additional on-call rotation
- #352629
- tweak local setup: CustomerDot not working

# 2022-02-28
- follow-up on https://gitlab.com/gitlab-org/gitlab/-/issues/342853#note_846739310
- #353921

# 2022-02-24
- sirt_2010
- review: !81078
- file expenses
- catastrophize about current events

# 2022-02-23
- read up on docker license change
- continue feature flag training
- #352629

# 2022-02-22
- meeting: 1:1 w/ Phil
- process slack
- process emails
- meeting: 1:1 w/ Kamil
- #352066
- follow-up on https://gitlab.com/gitlab-org/gitlab/-/issues/342853#note_846739310
- meeting: Growth Engineering Weekly

# 2022-02-21
- process slack
- process emails
- follow-up on https://gitlab.com/gitlab-org/gitlab/-/issues/342853#note_846739310
- #352066

# 2022-02-18
- process slack
- process emails
- manage RSUs
- follow-up on https://gitlab.com/gitlab-org/gitlab/-/issues/342853#note_846739310
- #352066

# 2022-02-17
- process emails
- process slack
- install & set up Toby tab extension
- #352066
- review: !80809

# 2022-02-16
- process emails
- process slack
- collaborate on gitlab-data/product-analytics/-/issues/158
- #352066
- meeting: Conversion 14.9 Kickoff

# 2022-02-15
- process slack
- process emails
- set up Clockwise integration
- try to set up CustomerDot locally again
- !80718
- meeting: 1:1 w/ Kamil
- coffee chat: Karlia Kue
- meeting: GitLab FY23 Kickoff

# 2022-02-14
- process slack
- finish review: !78624
- add report to #342579
- process email
- #346644
- try to setup CustomerDot locally again
- !80718

# 2022-02-11
- process slack
- #346644
- gitlab-data/product-analytics/-/issues/158
- fill out access request
- review: !78624

# 2022-02-10
- process emails
- process slack
- #346644
- follow-up review !79487
- read: How to deploy Shopify themes automatically (w/ GitLab)
  - refs: #292664 & !52279
- watch: WizVille conference talk by @dhh
- read: handbook section on experiments
- !80427
- !80363

# 2022-02-09
- continue GDK setup
- review: !79487
- attempt local GCP setup
- meeting: sync with Jannick about !79487
- meeting: sync with Nilesh about SaaS Signupflow Dashboard
- debug signup flow numbers
- coffee chat: Robin Hwang
- meeting: Growth Conversion Team Meeting
- finish review !79487

# 2022-02-08
- coffee chat: Charlie Ablett
- process emails
- process slack
- meeting: 1:1 w/ Kamil
- meeting: Frontend Department Meeting
- #346644
- meeting: Growth Weekly
- meeting: short 1:1 w/ Phil
- nuke GDK and re-install
- meeting: Growth Sub-department Engineering Weekly - AMER

# 2022-02-07
- process emails
- process slack
- clarify expense issue
- #345175
- meeting: discuss 345175 with Doug
- #346644

# 2022-02-04
- process emails
- read SaaS purchase flow PA QA results
- process slack
- fill form for hardware refresh
- review !79816
- coffee chat: Bastien Escudé
- help QA gitlab-data/product-analytics/-/issues/158
- #345175

# 2022-02-03
- process emails
- read: Growth Weekly minutes from 2022-02-01
- read: E-Group Offsite Pre-Read
- process slack
- read: GitLab mentorship program handbook page
- #345175

# 2022-02-02
- complete training: Building Empathy with Our Users
- collaborate on #346644
- collaborate on !79654
- collaborate on https://gitlab.com/conventionalcomments/conventionalcomments.gitlab.io/-/issues/22
- research external monitor
- #346644
- review: !52279

# 2022-01-28
- review: !79204
- #346644

# 2022-01-27
- process slack
- read: Handbook pages about OnCall rotations
- watch: recording of Growth Conversion Team Meeting
  - answer questions in notes document
- read: Vue.js & test utils documentation
- refactor #342853
- review: !79204

# 2022-01-26
- refactor #342853
- process emails
- process slack
- read: Handbook pages about OnCall rotations
- refactor #342853
- watch: Vue.js Nation 2022 talk by Natalia Tepluhina

# 2022-01-25
- process slack
- process emails
- get !77849 merged
- meeting: 1:1 w/ Kamil
- watch: Live Speaker Series - Lorraine Lee, All-Remote Presentation and Video Tips

# 2022-01-24
- process emails
- refactor #349804
- tweak local zsh
- process slack
- refactor #342853
- write retro notes

# 2022-01-21
- contribute to retrospective
- refactor #349804
- process slack
- collaborate on gitlab-com/marketing/community-relations/contributor-program/general/-/issues/116
- refactor #349804
- pair program: frontend sync pairing meeting
- refactor #349804

# 2022-01-20
- process email
- process slack
- read: 1:1 notes to prepare for next week
- watch: UX Showcase Security Config Evaluations
- refactor #342853
- refactor #349804

# 2022-01-19
- process email
- try to fix local snowplow micro instance configuration
- refactor #342853

# 2022-01-18
- meeting: 1:1:1 w/ Phil & Kamil
- process slack
- refactor #342853
- meeting: 1:1 w/ Doug, discussed:
  - how best to progress on #342853
  - best practices on being a reviewer

# 2022-01-17
- process emails
- struggle with/fix local GDK
- process slack
- collaborate on #342853
- refactor #342853
- attempt to fix local GDK/snowplow micro
- process slack
- read apollo documentation useMutation()
- refactor #342853

# 2022-01-14
- process emails
- discuss: semi-blocking technical debt issue for #342853 with Miguel
  - related: #350189
  - related: #350344
  - related: !78259
- review: !78076
- review: !78204

# 2022-01-13
- watch Growth Product Highlights Review & Roadmap
- #342853
- read Vue docs: scoped slots
- read Vue docs: $listener & $emit
- start Reliability training
- review: !78104
- review: !78076

# 2022-01-12
- gitlab-org/release/tasks/-/issues/3324
- meeting: Modern Health coaching session
- meeting: sync code review w/ Miguel
- #342853
- meeting: Growth Conversion Sync
- coffee chat: Stacy Johnson

# 2022-01-11
- meeting: 1:1 w/ Phil
- collaborate on #345175
- meeting: frontend department
- #343494
- #342853

# 2022-01-10
- #349804
- collaborate on #343179
- continue feature flag training
- #342853

# 2022-01-07
- fix local CustomersDot
- continue feature flag training
- collaborate on #349653
- read Changelog docs
- #349653

# 2022-01-06
- process email
- process slack
- collaborate on #349653
- read !76550
- #345175

# 2022-01-05
- process email
- #342853
- process slack
- #343494
- continue feature flag training
- collaborate on #342853
- #343494

# 2022-01-04
- meeting: GitLab Social Call
- process slack
- process emails
- #342853
- process slack

# 2022-01-03
- update macOS
- update GDK

# 2021-12-22
- collaborate on #343179
- coffee chat: Kamil Niechajewiscz
- write 14.6 retrospective notes
- #343494
- pair program w/ Doug on #347446
- #343494

# 2021-12-21
- collaborate on #343179
- #343494
- meeting: Conversion Engineering EMEA
- #343494
- process emails
- process slack
- collaborate on gitlab-org/incubation-engineering/mlops/rb-ipynbdiff/-/merge_requests/9
- #343494

# 2021-12-20
- process emails
- collaborate on #343179
- read review comments for !74828
- #343494

# 2021-12-17
- process emails
- #343494
- process slack

# 2021-12-16
- process emails
- #342853
- process slack
- #342853
- coffee chat: Jaimie Kander
- #342853

# 2021-12-15
- #343494
- fixing local GDK
  - migrations seem broken and won't let me run specs
- pair program with w/ Doug on #342853
- meeting: Growth Engineering Sync
- #342853

# 2021-12-14
- process emails
- meeting: 1:1 w/ Phil
- #342853
  - having a bit of trouble writing tests for a Vue component
    where I need to trigger a callback on a sub-component
- meeting: Growth Weekly

# 2021-12-13
- process slack
- process emails
- #342853

# 2021-12-10
- process slack
- read up on JS await/async
- #342853
  - had some configuration trouble with the payment service
    not delivering errors in dev mode, it was configured
    to use a dummy that will always return a success.
- meeting: Growth Social Call
- meeting: w/ Doug about testing of instrumentation

# 2021-12-09
- #342853
- process emails
- process slack

# 2021-12-08
- process emails
- #342853
- meeting: GitLab AMA with new board member
- meeting: GitLab Assembly earnings AMA

# 2021-12-07
- #342853
- process slack
- read #347142
- #342853

# 2021-12-06
- process slack
- process emails
- read internal stock policy guidelines
- #342853

# 2021-12-03
- process slack
- #342853
- read Growth End of Year Social Event issue
- pair program with w/ Doug on #342853

# 2021-12-02
- fix broken GDK
- process slack
- take a look into #346969
- #342853

# 2021-12-01
- update tmuxinator config
  - restarting the computer was a pain when having to restart all types of services:
    - gdk
    - snowplow
    - customersDot
    - guard
- #342853
- fix broken GDK

# 2021-11-30
- process emails
- prepare for 1:1
- meeting: 1:1 w/ Phil
- process emails
- review payslip for correctness
  - 2021-10 corrections are good
  - 2021-11 looks good
- process slack
- read Group conversations survey highlights
- update GDK
- meeting: Modern Health coaching session
- #342853
  - add instrumentation to Edit buttons in checkout
    still trying to figure out exactly where the edit prop gets passed to hook into that
- read handbook: List of Top Misused Terms

# 2021-11-24
- coffee chat: Ai Ling Kang
- process slack
- process emails
- #343494
  - tweak local GDK & CustomesDot integration
  - read up on spec helper IdempotentWorkerHelper
  - read up on let_it_be
  - write comment ask for feedback on implementation plan
- process emails
- process slack

# 2021-11-23
- meeting: 1:1 w/ Phil
- manage work log items
- review financial well-being service provider AMA
- #343494
- update GDK
- read https://gitlab.com/gitlab-com/gl-infra/production/-/issues/5931
- process slack
- answer slack question: research where number_of_users in a trial gets persisted
- #343494
- meeting: Growth Sub-department Engineering Weekly

# 2021-11-22
- investigate slow vim performance
- attempt coffee chat
- update personal README
- process slack
- collaborate on #327029
  - Looks like this issue was already worked on in a different and very similar issue
    commented to ask for clarification
- collaborate on #338536
- collaborate on #342853

# 2021-11-19
- email swag provider about FedEx invoice
- process emails
- process slack
- #342853
- process slack
- #342853
- pair program with w/ Doug on #342853
- write down questions on #342853

# 2021-11-18
- process emails
- take ITOps laptop survey
- process slack
- set up PTO for upcoming F&F days
- #342853
- collaborate on !72838
- #342853

# 2021-11-17
- #342853
- process emails
- process slack
- take part in Contribute 2021
- Pair with w/ Doug on #342853
- meeting: Growth Conversion Sync

# 2021-11-16
- process emails
- meeting: 1:1 w/ Phil
- update GDK
- fix Snowplow Micro setup
- #342853
  - Had local GDK problems with Snowplow not loading, recent change
    made an adaptation to the content policy necessary #345441
- take part in Contribute 2021

# 2021-11-15
- recover from illness

# 2021-11-12
- review !74302
- process slack
- process emails

# 2021-11-11
- read #322736
- collaborate on #345199
- watch DK farewell video
- re-attempt EE licesne setup in GDK

# 2021-11-10
- process emails
- coffee chat: Firdaws Farukh
- look up movie quote for Firdaws for https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/3814
- process slack
- set up EE license in GDK
  - GDK keeps rejecting the license for unknow reasons
- #342853
- pair program w/ Doug on #342853

# 2021-11-09
- meeting: 1:1 w/ Phil
- process emails
- meeting: frontend department
- read: Breaking changes in SASS concerning @extend
- process slack
- research & discuss #345199
- tinker with GitLab Alfred workflow
- fill out OSAT survey
- meeting: Experimentation AMA w/ Eduardo Bonet
- meeting: Growth sub-department Engineering Weekly AMER/EMEA
- read Vue JS documentation

# 2021-11-08
- process emails
- process slack
- read: Frontend Vue testing guidelines
- meeting: Modern Health coaching session
- #342853
- pair program w/ Doug on #342853
- coffee chat: Jacki Bauer

# 2021-11-05
- process emails
- take Engagement Survey 2021 - Pulse
- process slack
- read about CVE-2021-22205 situation
- review Wayne Haber Office Hours AMA
- take Group Conversations survey
- finish CustomersDot setup

# 2021-11-04
- process emails
- process slack
- continue CustomersDot setup
- review past Growth sub-department weeklies
- coffee chat: Doug Stull

# 2021-11-03
- add technical writing to fullstack onboarding
- process email
- process slack
- finish sorting out health insurance
- continue CustomersDot setup
- meeting: technical writing sync coaching
- continue DI&B course on GitLab Learn
- meeting: Growth::Conversion sync EMEA
- finish DI&B course on GitLab Learn
- continue CustomersDot setup

# 2021-11-02
- meeting: Social Call
- meeting: 1:1 w/ Phil
- inquire about CloudPay again
- coffee chat: Alex Buijs
- set up CustomersDot
- coffee chat: Katie Corso
- set up CloudPay account
- sort payroll mishap
- sort health insurance auto signup

# 2021-10-29
- read slack thread about GLEX terms
  - persist in Google Doc for later reference or to incorporate it into the GLEX docs
- process emails
- review key points from Technical Writing Training for retention
- inquire about missing CloudPay onboarding
- #327029

# 2021-10-28
- submit expense reports
- continue Iteration learning issue
- process emails
- try to deal with solution website for employee benefits program
- process slack
- finish Iteration learning issue
- complete the Backward Compatibility Training in GitLab Learn
- start Feature Flag Training issue
- process emails
- process slack
- watch Technical Writing videos on GitLab Learn

# 2021-10-27
- set up missing steps in employee benefits program
- continue Iteration learning issue
- catch up on slack
- start DIB course
- clean up open tabs
  - literally had 100+ open from various onboarding items
    from the last 3.5 weeks
- complete GitLab Regulation FD Training on GitLab learn
- update dotfiles
- #327029

# 2021-10-26
- open MR for #327029
- more setup of account with brokerage for employee benefits
- meeting: 1:1 w/ Phil
- coffee chat: Joanna Michniewicz
- read growth developer onboarding pages linked in issue
- catch up on slack
- sorting HR issue w/ health insurance
- check off more role based onboarding
- read into snowplow micro
- continue onboarding issue
- meeting: Growth Engineering Weekly
- finish GitLab 101 on GitLab Learn

# 2021-10-25
- set up new microphone stand
- read GLEX readme
- reading Design Management page
- finish remote foundations on GitLab Learn
- catch up on slack
- finish missing calendly set up steps
- set up TripAction
- meeting: Modern Health coaching session
- set up account with brokerage for employee benefits
- processing emails
- fix GDK setup
- work on Iteration training issue

# 2021-10-22
- checking up on onboarding items I've already done and checking them off
- read communcation guide in handbook
- set up calendly
- set up & request ChatOps access
- read testing best practices
- trying out a fix for `badge-pill` border radius issues
  - searching for existing issues
- #338766
- set up new desk hardware that arrived today
- coffee chat: Matt Allen
- meeting: employer branding/hiring survey/interview


# 2021-10-21
- reading async retrospective notes
- reading into secure code training issue
- catching up on slack
- setting up Modern Health
- reading more Snowplow documentation
- sort out missing Tipalti on-boarding
- meeting: new employee benefits [details kept SAFE] for GmbH employees
- give input on "Truncate Rails logs before update" https://gitlab.com/gitlab-org/gitlab-development-kit/-/merge_requests/2220
- reschedule upcoming meeting
- experiment with kitty terminal
- coffee chat: Donald Cook
- researching branch naming conventions
  - from asking around it seems we don't have a convention per se
- meeting: new employee benefits AMA [details kept SAFE]

# 2021-10-20
- re-reading "Product Development Flow"
- fix typos in "Product Development Flow" and created MR
- adjusted some Zoom settings according to the handbook
- corresponding with Talent Brand Program Manager Cali Chiodo about hiring branding survey done with external agency
  - setup meeting for 2021-10-22 18:00 UTC+1
- process emails
- process slack threads
- read up on new employee benefits [details kept SAFE]
- read up on `review-qa` & `review-qa-smoke`, and `package-and-qa changes`
- ordered desk equipment after pre-approval
- meeting: Growth conversion sync EMEA
- coffee chat: Dan Croft
- read Java @Overrides concept
  - read our Gitlab::Utils::Override::Extension


# 2021-10-19
- setting up GDK:
  - finally got it running, had to reset everything and manually brew install OpenSSL to get it running
  - reading documentation for GDK
  - testing some of the commands
- meeting: 1:1 with Phil Calder
- meeting: Frontend Dep Meeting
  - afterwards reading up on changes/deprecations:
    - getJSONFixture
    - wrapper.vm in testing
- set up mail filters and labels to make processing emails more efficient
- reading iglu, igluctl documentation and related JSON Schema docs
- coffee chat: Kevin Camoli
- slack: equipment inquiry for desk setup
- started personal readme
- started personal project on gitlab.com work_log (this repo) to keep track of things I need to do
  - I was previously trying to keep it all in email and GSuite tasks, but it isn't very practical
- fix vim config for spellcheck in markdown files

