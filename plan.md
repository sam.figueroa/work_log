# 2025-02-28
- meeting: coffee chat w/ Lohit Peesapati
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/182870 
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/182712
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/182877
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/425324

# 2025-02-26
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/512381
- meeting: Kaffeekraenzchen
- fix: https://gitlab.com/gitlab-org/gitlab/-/issues/515452
- continue investigation: https://gitlab.com/gitlab-org/gitlab/-/issues/503968#note_2364450118
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174039

# 2025-02-25
- meeting: 1:1 w/ Nate
- meeting: coffee chat w/ Inma Checa
- meeting: Compliance Weekly
- meeting: coffee chat w/ Jörn
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174039

# 2025-02-24
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174039

# 2025-02-21
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/507239
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/177748
- rollout ff on staging: https://gitlab.com/gitlab-org/gitlab/-/issues/503968
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174039

# 2025-02-20
- process emails
- process todos
- review MRs outstanding
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174039

# 2024-12-18
- finish: https://gitlab.com/gitlab-org/gitlab/-/issues/499047
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174996

# 2024-12-16
- update MacOS
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/470695#note_2247726052
- fix specs: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/173584
- fix rollback error: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174996
- update milestone planning

# 2024-12-09
- finish: https://gitlab.com/gitlab-org/gitlab/-/issues/499047
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/174484

# 2024-12-02
- process email
- https://gitlab.com/gitlab-org/gitlab/-/issues/505488
- https://gitlab.com/gitlab-org/gitlab/-/issues/503968
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/480117

# 2024-11-26
- meeting: 1:1 w/ Nate
- meeting: Compliance Weekly
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/480117
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/169338

# 2024-11-25
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/169338
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/480117
- planning: https://gitlab.com/gitlab-org/gitlab/-/issues/464122

# 2024-11-18
- finish: https://gitlab.com/gitlab-org/gitlab/-/issues/433329
- finish: https://gitlab.com/gitlab-org/gitlab/-/issues/478560
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/480117

# 2024-11-15
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/480117

# 2024-11-14
- handle: update Workday profile request
- catch up on todos
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/171745
- https://gitlab.com/gitlab-org/gitlab/-/issues/478560
- https://gitlab.com/gitlab-org/gitlab/-/issues/480460
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/480117

# 2024-11-11
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/480117

# 2024-11-08
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/171720
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/480117
- update mantainership doc
- catch up on todos
- handle: update Workday profile request

# 2024-11-07
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/170380
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/171720
- update mantainership doc
- catch up on todos
- handle: update Workday profile request

# 2024-11-06
- update mantainership doc
- catch up on todos
- handle: update Workday profile request
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/481325
- start: https://gitlab.com/gitlab-org/gitlab/-/issues/440922

# 2024-11-05
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/171345
- meeting: 1:1 w/ Nate
- meeting: coffee chat w/ Yasha Rise
- meeting: Compliance Weekly
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/171181
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/481325
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/165160
- handle: update Workday profile request
- catch up on todos
- start: https://gitlab.com/gitlab-org/gitlab/-/issues/440922

# 2024-11-04
- process feedback: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168865
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/170200
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/170971
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/480117

# 2024-10-30
- process feedback: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168865
- meeting: Sec coffee chat
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/170200
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/170971

# 2024-10-29
- meeting: 1:1 w/ Nate
- finish cleanup: ff_compliance_audit_mr_merge
- meeting: Compliance Weekly
- process feedback and fix CI: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168865
- meeting: coffee chat w/ Andrew Jung

# 2024-10-28
- finish review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/143914
- process feedback and fix CI: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168865

# 2024-10-23
- finish: https://gitlab.com/gitlab-org/gitlab/-/issues/472145
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/169165
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/169485

# 2024-10-22
- record: Compliance Demo
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/169165
- meeting: 1:1 w/ Nate
- cleanup: ff_compliance_audit_mr_merge
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/169485
- meeting: Compliance Weekly
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/472145

# 2024-10-21
- enable ff_compliance_audit_mr_merge globally on gprd
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/472145

# 2024-10-18
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/169452
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/169287
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/472145

# 2024-10-17
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/169452
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/169287
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/472145

# 2024-10-16
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/169330
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/169352
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/472145
- deploy: feature flag for https://gitlab.com/gitlab-org/gitlab/-/issues/442279
- start: https://gitlab.com/gitlab-org/gitlab/-/issues/440922

# 2024-10-15
- finish local k8s setup
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168626
- prepare 1:1 agenda
- meeting: 1:1 w/ Nate
- prepare to host Compliance Weekly
- meeting: Compliance Weekly
- review: https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/4535
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/472145

# 2024-10-14
- finish: https://gitlab.com/gitlab-org/gitlab/-/issues/442279
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168799
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168626
- review: https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/4535
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/472145

# 2024-10-11
- finish: https://gitlab.com/gitlab-org/gitlab/-/issues/442279
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/472145

# 2024-10-10
- finish: https://gitlab.com/gitlab-org/gitlab/-/issues/442279
- record: Compliance Demo
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/472145

# 2024-10-09
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168508
- finish: https://gitlab.com/gitlab-org/gitlab/-/issues/442279

# 2024-10-08
- prepare 1:1 agenda
- meeting: 1:1 w/ Nate
- meeting: Compliance Weekly
- finish: https://gitlab.com/gitlab-org/gitlab/-/issues/442279

# 2024-10-07
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/167933
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/167021
- update OS
- record: Compliance Demo
- finish: https://gitlab.com/gitlab-org/gitlab/-/issues/442279

# 2024-10-02
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/167510
- finish: https://gitlab.com/gitlab-org/gitlab/-/issues/442279

# 2024-10-01
- meeting: 1:1 w/ Nate
- meeting: Compliance Weekly
- record: Compliance Demo
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/167510
- finish: https://gitlab.com/gitlab-org/gitlab/-/issues/442279

# 2024-09-10
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/442279
- meeting: Compliance Weekly

# 2024-09-03
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/442279
- meeting: Compliance Weekly

# 2024-07-10
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158395
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/158661
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/44092
# 2024-06-27
- fill out: Compliance: domain knowledge survey

# 2024-06-27
- meeting: Backend Pairing
- review: https://gitlab.com/gitlab-korg/gitlab/-/merge_requests/156632 again
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/155652
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/440922
- stretch: write specs to continue https://gitlab.com/gitlab-org/gitlab/-/merge_requests/155754

# 2024-06-26
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/156632
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/157285
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/157228
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/155652
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/440922
- stretch: write specs to continue https://gitlab.com/gitlab-org/gitlab/-/merge_requests/155754

# 2024-06-11
- meeting: 1:1 w/ Nate
- meeting: Compliance Weekly
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/155769
- fix build for: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/150710
- continue (if time left: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/155652)

# 2024-06-10
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/155791
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/424387
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/155652

# 2024-05-21
- meeting: coffee chat w/ Daniel Abeles
- meeting: coffee chat w/ Flavia Costa
- meeting: 1:1 w/ Nate
- meeting: Compliance Weekly
- meeting: coffee chat w/ Marion Daire
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/150710

# 2024-05-14
- meeting: 1:1 w/ Nate
- meeting: Compliance Weekly
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/431346

# 2024-04-30
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/431346
- meeting: 1:1 w/ Nate
- meeting: coffee chat w/ Joern Schneeweisz
- meeting: Compliance Weekly
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/436614

-# 2024-04-25
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/431346
- add backports for sec fix

# 2024-04-08
- push sec fix review
- Open MR for API changes

# 2024-03-25
- coffee chat: Ian Khor
- open MR for sec fix
- API changes

# 2024-03-05
- meeting: 1:1 w/ Nate
- announce: https://gitlab.com/gitlab-com/content-sites/handbook/-/merge_requests/3688 on slack
- test group level audit event streaming for Cells POC
- test SAML SSO MR approval for Cells POC
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/424387

# 2024-02-27
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/442064
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/42438776j

# 2024-02-26
- review: https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/3899
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/424387

# 2024-02-23
- continue: NN-zero to hero course [timebox 3600 s]

# 2024-02-22
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/424387
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/145302

# 2024-02-21
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/424387
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/145302

# 2024-02-16
- review Cells documentation

# 2024-02-15
- start: https://gitlab.com/gitlab-org/gitlab/-/issues/421636

# 2024-02-13
- fill out: growth plan
- prepare: 1:1 agenda
- meeting: 1:1 w/ Nate
- meeting: coffee chat w/ Dominique Top
- meeting: coffee chat w/ Sarina Kraft
- meeting: coffee chat w/ Olivier Gonzales
- process emails
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/431714

# 2024-02-12
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/431714
- process emails

# 2024-02-09
- start: https://gitlab.com/gitlab-org/gitlab/-/issues/421636
- start clean up: https://gitlab.com/gitlab-org/gitlab/-/issues/431714

# 2024-02-08
- meeting: 1:1 w/ Nate
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/435713
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/144043
- start: https://gitlab.com/gitlab-org/gitlab/-/issues/421636
- start clean up: https://gitlab.com/gitlab-org/gitlab/-/issues/431714

# 2024-02-07
- meeting: backend pairing
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/435713

# 2024-02-06
- close: https://gitlab.com/gitlab-org/gitlab/-/issues/424447
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/143247

# 2024-02-02
- process reviews
- meeting: compliance pairing

# 2024-02-01
- create feature flag issues for compliance pipeline maintenance & deprecation

# 2024-01-31
- meeting: coffee chat w/ Hitesh
- finish: compliance pipeline maintenance & deprecation plan
- meeting: Kaffeekraenzchen
- process reviews
- process emails

# 2024-01-30
- meeting: 1:1 w/ Nate
- meeting: Compliance Bi-Weekly
- plan: compliance pipeline maintenance & deprecation
- process emails

# 2024-01-29
- plan: compliance pipeline maintenance & deprecation

# 2024-01-26
- plan: compliance pipeline maintenance & deprecation
- meeting: compliance pairing

# 2024-01-25
- plan: compliance pipeline maintenance & deprecation

# 2024-01-24
- meeting: 1:1 w/ Nate
- process reviews

# 2024-01-18
- meeting: 1:1 w/ Nate
- meeting: Compliance Bi-Weekly

# 2024-01-16
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/435404#note_1727872073
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/141532

# 2024-01-15
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/139912
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/140979
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/141710
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/435404
- plan: https://gitlab.com/gitlab-org/gitlab/-/issues/426660

# 2024-01-10
- meeting: backend pairing
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/435404

# 2024-01-04
- file expenses
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/435404

# 2024-01-03
- meeting: backend pairing
- push assigned MRs forward

# 2024-01-02
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/418197
- process emails
- coffee chat

# 2023-12-19
- continue https://gitlab.com/gitlab-org/gitlab/-/issues/435713
- meeting: 1:1 w/ Nate
- meeting: Compliance Bi-Weekly

# 2023-12-11
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/431415
- meeting: pairing on Instance SAML approval

# 2023-11-29
- meeting: backend pairing
- write implementation plan for instance SAML MR approval
- push review for https://gitlab.com/gitlab-org/gitlab/-/merge_requests/137067
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/137893

# 2023-11-28
- write implementation plan for instance SAML MR approval
- push review for https://gitlab.com/gitlab-org/gitlab/-/merge_requests/137067
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/137893

# 2023-11-20
- write retro notes:
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/137067

# 2023-11-06
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- meeting: compliance pairing

# 2023-11-03
- meeting: 1:1 w/ Nate
- meeting: compliance pairing
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959

# 2023-10-30
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- post shortcut work
- process verification queue

# 2023-10-27
- meeting: compliance pairing
- do follow-ups

# 2023-10-26
- process feedback: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129608
- coffee chat: Drew Blessing

# 2023-10-25
- meeting: backend pairing
- meeting: Compliance Bi-Weekly
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- process feedback: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129608
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/135059
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/133648

# 2023-10-24
- meeting: 1:1 w/ Nate
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- participate in: GitLab Duo Chat Bash

# 2023-10-23
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- create: Compliance violation export demo

# 2023-10-20
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- meeting: compliance pairing

# 2023-10-19
- process emails
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959

# 2023-10-18
- coffee chat: James Hebden
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- meeting: backend pairing

# 2023-10-17
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- coffee chat: w/ Phil
- meeting: 1:1 w/ Nate
- meeting: Book Club: Impact Players

# 2023-10-16
- release compliance_violation_csv_export FF to default true
- fill: talent assesment doc

# 2023-10-13
- read: https://docs.gitlab.com/ee/development/internal_analytics/
- fill: talent assesment doc

# 2023-10-12
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- roll out FF for violation export to 75%

# 2023-10-11
- meeting: backend pairing
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- roll out FF for violation export to 50%

# 2023-10-10
- prepare: Compliance Sync agenda
- meeting: 1:1 w/ Nate
- meeting: Compliance Bi-Weekly
- meeting: Book Club: Impact Players
- review: !129702
- process notes from 1:1
- roll out FF for violation export to 25%
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959

# 2023-09-27
- meeting: backend pairing

# 2023-09-19
- meeting: 1:1 w/ Nate
- coffee chat: Kamil
- meeting: Book Club: Impact Players
- hack: compliance hackathon

# 2023-09-13
- coffee chat: Fabiene Catteau
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/131206

# 2023-09-12
- prepare: Compliance Sync agenda
- meeting: 1:1 w/ Nate
- meeting: Compliance Bi-Weekly
- meeting: compliance pairing
- add Compliance Center metrics to SiSense

# 2023-09-07
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/423263#note_1544134017
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/120636
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959

# 2023-09-06
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/423263#note_1544134017
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129532
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/125512

# 2023-09-05
- 2 coffee chats
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/120636
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/125512

# 2023-09-01
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/130599
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/120636
- continue: NN-zero to hero course [timebox 3600 s]

# 2023-08-31
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/128148
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/120636

# 2023-08-31
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/128148
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/120636

# 2023-08-30
- 5 meetings
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129780
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/120636
- update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/128148

# 2023-08-29
- process feedback on open MRs
  - update: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129780
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/120636
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959

# 2023-08-28
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/130079
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/120636
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- process feedback on open MRs

# 2023-08-24
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129899
- start: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/120636

# 2023-08-23
- meeting: backend pairing
- continue: setup: local SAML IdP
- process: review feedback
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/408151
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129899
- start: https://gitlab.com/gitlab-org/gitlab/-/issues/421959

# 2023-08-22
- start: https://gitlab.com/gitlab-org/gitlab/-/issues/421959
- prepare 1:1 agenda
- meeting: 1:1 w/ Nate
- check progress on open MRs
- process emails
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/408151
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/128313

# 2023-08-21
- start: https://gitlab.com/gitlab-org/gitlab/-/issues/421944
- process review feedback: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129261
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/128148 - Add specs for tracking, check if different mount type helps
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129699 - fix specs failing on CI
- process emails
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129608

# 2023-08-18
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129595
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129532
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/374110 [timebox 7200 s]
- continue: NN-zero to hero course [timebox 3600 s]

# 2023-08-17
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/408153
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129261
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/250663

# 2023-08-15
- meeting: 1:1 w/ Nate
- meeting: Compliance Bi-Weekly
- coffee chat: Cassiana

# 2023-08-14
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/377625
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/419733

# 2023-08-11
- hack: compliance hackathon

# 2023-08-10
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/408153
- hack: compliance hackathon

# 2023-08-09
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/408153
- meeting: backend pairing
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/370169

# 2023-08-08
- prepare 1:1 agenda
- meeting: 1:1 w/ Nate
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/128134
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/408153#note_1502153455
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/419733
- coffee chat: Cassiana

# 2023-08-07
- continue verify: https://gitlab.com/gitlab-org/gitlab/-/issues/417433
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/419733

# 2023-08-04
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/417433#note_1498889277
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/374110 [timebox 7200 s]
- continue: NN-zero to hero course [timebox 3600 s]
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/120636

# 2023-08-03
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/98438
- meeting: compliance paring
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/128147

# 2023-08-02
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/411557
- copy growth plan objectives to sticky notes
- copy OKR to sticky notes
- meeting: backend pairing
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/98438

# 2023-08-01
- prepare 1:1 agenda
- meeting: 1:1 w/ Nate
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/98438
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/419733

# 2023-07-31
- process emails
- process slack
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/98438

# 2023-07-26
- meeting: backend pairing
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/98438
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/374110

# 2023-07-25
- prepare 1:1 agenda
- meeting: 1:1 w/ Nate
- meeting: coffee chat w/ Chinmay Mehrotra
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/98438
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/374110

# 2023-07-24
- file expenses
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/126930
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/98438

# 2023-07-21
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/370169
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/259159
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/98438

# 2023-07-20
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/126930
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/125870
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/259159
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/370169

# 2023-07-19
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/125870
- read: https://gitlab.com/gitlab-org/gitlab/-/issues/377725
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-07-18
- prepare: Compliance Bi-Weekly (hosting)
- meeting: 1:1 w/ Nate
- meeting: Compliance Bi-Weekly
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-07-17
- process emails
- process slack
- update GDK
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-06-29
- add reoccurring slack alert for future compliance pairing meetings
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791
- create: coverage issue
- process emails

# 2023-06-28
- create: coverage issue
- add reoccurring slack alert for future compliance pairing meetings
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/415194
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-06-27
- fill out: mid-year check-in
- fill out: retrospective
- prepare: 1:1
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/415194
- meeting: 1:1 w/ Nate
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-06-26
- fill out: mid-year check-in
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/415194
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-06-22
- process review feedback
- continue review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/122931
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/124177
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/124268

# 2023-06-21
- process emails
- process review feedback
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/124177
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/124268
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/122931

# 2023-06-20
- prepare 1:1 agenda
- meeting: 1:1 w/ Nate
- meeting: 1:1 w/ Camelia
- meeting: Compliance Bi-Weekly
- file expenses
- process review feedback

# 2023-06-19
- file expenses
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/374110
- process review feedback

# 2023-06-16
- process review feedback
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/374110

# 2023-06-15
- process review feedback
- meeting: compliance pairing
- coffee chat: Doug
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/374110

# 2023-06-14
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/122521
- coffee chat: Charlie
- coffee chat: Sara Cade
- coffee chat: Doug
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791
  - add frontend UI

# 2023-06-13
- prepare 1:1 agenda
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/374110
- meeting: 1:1 w/ Nate
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-06-12
- read: essentialism for 15 min
- start ClickHouse training: https://learn.clickhouse.com/visitor_class_catalog/category/116050
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/122255
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/122746

# 2023-06-07
- read: essentialism for 15 min
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791
- update compliance bugs OKR
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/113

# 2023-06-06
- plan 1:1 agenda
- elaborate OBS recording setup
- meeting: 1:1 w/ Nate
- meeting: Compliance Bi-Weekly
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-06-05
- start ClickHouse training: https://learn.clickhouse.com/visitor_class_catalog/category/116050
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791
- elaborate OBS recording setup
- complete: annual security training

# 2023-06-02
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/408495
- file expenses

# 2023-06-01
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791
- elaborate OBS recording setup
- do training listed on: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/104
- pairing

# 2023-05-31
- elaborate OBS recording setup
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/111
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/121739
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/121602
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-05-30
- meeting: 1:1 w/ Nate
- coffee chat: Hoossam Hamdy
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/121739
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-05-25
- continue: secure code training
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/121739
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/119891
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-05-24
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791
- coffee chat: w/ Gayle
- coffee chat: w/ Chiemi
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/410311

# 2023-05-23
- prepare 1:1 agenda
- meeting: 1:1 w/ Nate
- meeting: Compliance Bi-Weekly
- meeting: 1:1 w/ Thaigo
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-05-22
- update MacOS
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/118690
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/109#note_1398984197
- elaborate OBS recording setup

# 2023-05-17
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/409421
- create demo for compliance framework export
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-05-15
- post status updates on in progress isssues
- create demo for compliance framework export
- fix GDK (postgres update)
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791
- coffee chat: Jan Kunzmann

# 2023-05-12
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/120137
- start: secure code training
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-05-11
- pairing: w/ Jay
- process emails
- create demo for compliance framework export
- plan: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-05-09
- process emails
- process todos
- create demo for compliance framework export
- plan: https://gitlab.com/gitlab-org/gitlab/-/issues/356790
- plan: https://gitlab.com/gitlab-org/gitlab/-/issues/356791
- read: https://gitlab.com/groups/gitlab-org/-/epics/4367

# 2023-05-08
- meeting: 1:1 w/ Nate
- read: essentialism for 15 min
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/100#note_1381159127
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/115157
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/393848
- meeting: Compliance Bi-Weekly
- plan: https://gitlab.com/gitlab-org/gitlab/-/issues/356790
- plan: https://gitlab.com/gitlab-org/gitlab/-/issues/356791 # strech
- read: https://gitlab.com/groups/gitlab-org/-/epics/4367 # strech

# 2023-05-08
- read: essentialism for 15 min
- process MR feedback: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/119783
- read: https://gitlab.com/gitlab-org/govern/compliance/general/-/issues/100#note_1381159127
- plan: https://gitlab.com/gitlab-org/gitlab/-/issues/356790
- plan: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-05-05
- file expenses
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/119408
- install OS update
- read: essentialism for 15 min
- plan: https://gitlab.com/gitlab-org/gitlab/-/issues/356790
- plan: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-05-04
- fix: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/117471
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/114991
- process email
- plan: https://gitlab.com/gitlab-org/gitlab/-/issues/356790
- plan: https://gitlab.com/gitlab-org/gitlab/-/issues/356791

# 2023-05-03
- process feedback: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/118373
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/114991
- analyze: https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/2593

# 2023-05-02
- meeting: 1:1 w/ Nate
- coffe chat: Łukasz Korbasiewicz
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/387912
- coffe chat: Roberta Costi

# 2023-04-28
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/118612
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/387912

# 2023-04-27
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/118119
- fix: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/117471
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/387912

# 2023-04-26
- meeting: 1:1 w/ Nate
- meeting: 1:1 w/ Gayle
- meeting: Kaffeekraenzchen
- fix: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/117471
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/387912

# 2023-04-25
- process MR feedback on https://gitlab.com/gitlab-org/gitlab/-/merge_requests/117471
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/387912
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/118119

# 2023-04-24
- file expenses
- process MR feedback

# 2023-04-21
- fix GDK
- fix: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/117471
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/387912

# 2023-04-20
- process feedback: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/117471
- continue: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/114991

# 2023-04-18
- meetings
- update carreer development plan
- process verify queue

# 2023-04-13
- fix build on https://gitlab.com/gitlab-org/gitlab/-/merge_requests/117471
- process verify queue
- meetings

# 2023-04-12
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/388686
- open MR for https://gitlab.com/gitlab-org/gitlab/-/issues/388686
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/387911
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/394630
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/374110#note_1330946090

# 2023-04-06
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/388686
- open MR for https://gitlab.com/gitlab-org/gitlab/-/issues/388686
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/387911
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/394630
- update: https://gitlab.com/gitlab-org/gitlab/-/issues/374110#note_1330946090

# 2023-04-05
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/388686
- open MR for https://gitlab.com/gitlab-org/gitlab/-/issues/388686
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/387911

# 2023-04-04
- meeting: 1:1 w/ Nate
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/116541
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/388686

# 2023-04-03
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/388686
- add audit event to S3 lambda to https://gitlab.com/groups/gitlab-org/-/epics/6188

# 2023-03-31
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/388686

# 2023-03-28
- write issue for audit ee vs FOSS

# 2023-03-27
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/115537
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/115527
- continue: https://gitlab.com/gitlab-org/gitlab/-/issues/388686
- prepare for trip to Amsterdam

# 2023-03-23
- process emails
- start: https://gitlab.com/gitlab-org/gitlab/-/issues/388686
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/115537

# 2023-03-22
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/115144
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/374112#note_1322089651
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/393446#note_1321260528
- start: https://gitlab.com/gitlab-org/gitlab/-/issues/388686

# 2023-03-21
- 1:1 w/ Nate
- process feedback on: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/111312
- review: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/115144
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/393446#note_1321260528
- verify: https://gitlab.com/gitlab-org/gitlab/-/issues/374112#note_1322089651
- start: https://gitlab.com/gitlab-org/gitlab/-/issues/388686
- push review for: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/115221
